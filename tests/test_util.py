import unittest
import typing
from zabu.util import extract_key_value_pairs
from zabu.util import Iterator
from zabu.util import create_config_with_values_from_dict


class TestIterator(unittest.TestCase):

    def setUp(self):
        self.elements = ["a", "b", "c"]

    def test_empty_iterator_raises(self):
        with self.assertRaises(StopIteration):
            next(Iterator([]))

    def test_raises_at_end_of_list(self):
        iterator = Iterator(self.elements[0:1])
        next(iterator)

        with self.assertRaises(StopIteration):
            next(iterator)

    def test_iter_returns_self(self):
        iterator = Iterator([])

        self.assertIs(iterator, iter(iterator))

    def test_returns_all_elements(self):
        iterator = Iterator(self.elements)

        self.assertEqual(self.elements, list(iterator))

    def test_start_index_zero_returns_all_elements(self):
        self.assertEqual(self.elements, list(Iterator(self.elements, 0)))

    def test_start_index_in_list_returns_remaining_elements(self):
        self.assertEqual(self.elements[1:], list(Iterator(self.elements, 1)))

    def test_start_index_at_end_raises(self):
        iterator = Iterator(self.elements, len(self.elements))

        with self.assertRaises(StopIteration):
            next(iterator)

    def test_start_index_after_end_raises(self):
        iterator = Iterator(self.elements, len(self.elements) + 1)

        with self.assertRaises(StopIteration):
            next(iterator)

    def test_original_does_not_affect_copy(self):
        original = Iterator(self.elements)
        copy = original.copy()

        self.assertEqual(list(original), list(copy))

    def test_copy_does_not_affect_original(self):
        original = Iterator(self.elements)
        copy = original.copy()

        self.assertEqual(list(copy), list(original))

    def test_peek_returns_first_element(self):
        iterator = Iterator(self.elements)

        self.assertEqual(self.elements[0], iterator.peek())

    def test_peek_doesnt_change_the_iterator(self):
        original = Iterator(self.elements)
        copy = original.copy()

        copy.peek()
        copy.peek()

        self.assertEqual(list(original), list(copy))

    def test_peek_returns_element_of_advanced_iterator(self):
        iterator = Iterator(self.elements)
        next(iterator)

        self.assertEqual(self.elements[1], iterator.peek())

    def test_peek_at_end_of_list_raises(self):
        iterator = Iterator(self.elements, len(self.elements))

        with self.assertRaises(StopIteration):
            iterator.peek()


class _Config(typing.NamedTuple):
    name: str = ""
    number: int = 0
    names: typing.List[str] = []
    numbers: typing.List[int] = []
    boolean: bool = False


def _create_config(values):
    return create_config_with_values_from_dict(values, _Config)


class TestCreateConfigWithValuesFromDict(unittest.TestCase):

    def test_creates_config_with_string(self):
        values = {"name": "abc"}
        config = _Config(name="abc")

        self.assertEqual(config, _create_config(values))

    def test_creates_config_with_positive_int(self):
        values = {"number": "42"}
        config = _Config(number=42)

        self.assertEqual(config, _create_config(values))

    def test_creates_config_with_negative_int(self):
        values = {"number": "-42"}
        config = _Config(number=-42)

        self.assertEqual(config, _create_config(values))

    def test_creates_config_with_true_bool(self):
        config = _Config(boolean=True)

        for value in ["True", "true", "yes", "on"]:
            with self.subTest(true_string=value):
                values = {"boolean": value}
                self.assertEqual(config, _create_config(values))

    def test_creates_config_with_false_bool(self):
        config = _Config(boolean=False)

        for value in ["False", "false", "no", "off"]:
            with self.subTest(true_string=value):
                values = {"boolean": value}
                self.assertEqual(config, _create_config(values))

    def test_creates_config_with_list_of_strings(self):
        values = {"names": ["abc", "def"]}
        config = _Config(names=["abc", "def"])

        self.assertEqual(config, _create_config(values))

    def test_creates_config_with_list_of_ints(self):
        values = {"numbers": ["2", "3", "23"]}
        config = _Config(numbers=[2, 3, 23])

        self.assertEqual(config, _create_config(values))

    def test_creates_config_with_multiple_values(self):
        values = {"numbers": ["42", "13"], "boolean": "off", "name": "xyz"}
        config = _Config(numbers=[42, 13], boolean=False, name="xyz")

        self.assertEqual(config, _create_config(values))


class _Node(typing.NamedTuple):
    name: str
    content: typing.Union[list, str]


class TestExtractKeyValuePairs(unittest.TestCase):

    @staticmethod
    def _create_key_value_pair(key, values):
        def create_node_with_lexeme(node, lexeme):
            return _Node(node, [_Node("lexeme", lexeme)])

        def create_value_node(value):
            if value.startswith("'") and value.endswith("'"):
                return _Node("sqvalue", [_Node("lexeme", "'"),
                                         _Node("lexeme", value.strip("'")),
                                         _Node("lexeme", "'")])

            return create_node_with_lexeme("value", value)

        return _Node("keyvalue", [
            create_node_with_lexeme("key", key),
            _Node("lexeme", "="),
            *[create_value_node(value) for value in values],
            create_node_with_lexeme("eol", "\n")])

    def test_empty_tree_returns_empty_dict(self):
        self.assertEqual({}, extract_key_value_pairs([]))

    def test_returns_tree_with_single_pair(self):
        tree = [self._create_key_value_pair("abc", ["def"])]
        dict_ = {"abc": "def"}

        self.assertEqual(dict_, extract_key_value_pairs(tree))

    def test_returns_tree_with_multiple_pairs(self):
        tree = [self._create_key_value_pair("abc", ["def"]),
                self._create_key_value_pair("xyz", ["123"])]
        dict_ = {"abc": "def", "xyz": "123"}

        self.assertEqual(dict_, extract_key_value_pairs(tree))

    def test_returns_pair_with_multiple_values(self):
        tree = [self._create_key_value_pair("abc", ["def", "ghi"])]
        dict_ = {"abc": ["def", "ghi"]}

        self.assertEqual(dict_, extract_key_value_pairs(tree))

    def test_returns_tree_with_multiple_pairs_with_multiple_values(self):
        tree = [self._create_key_value_pair("a", ["b", "c"]),
                self._create_key_value_pair("x", ["y", "z"])]
        dict_ = {"a": ["b", "c"], "x": ["y", "z"]}

        self.assertEqual(dict_, extract_key_value_pairs(tree))

    def test_ignores_other_nodes(self):
        tree = [self._create_key_value_pair("a", "1"),
                _Node("comment", ""),
                self._create_key_value_pair("b", "2")]
        dict_ = {"a": "1", "b": "2"}

        self.assertEqual(dict_, extract_key_value_pairs(tree))

    def test_returns_single_quoted_value(self):
        tree = [self._create_key_value_pair("abc", ["'def'"])]
        dict_ = {"abc": "def"}

        self.assertEqual(dict_, extract_key_value_pairs(tree))

    def test_returns_multiple_quoted_values(self):
        tree = [self._create_key_value_pair("abc", ["'def'"]),
                self._create_key_value_pair("xyz", ["'123'", "'456'"])]
        dict_ = {"abc": "def", "xyz": ["123", "456"]}

        self.assertEqual(dict_, extract_key_value_pairs(tree))

    def test_returns_plain_and_quoted_values(self):
        tree = [self._create_key_value_pair("abc", ["'def'", "ghi"]),
                self._create_key_value_pair("xyz", ["123", "'456'"])]
        dict_ = {"abc": ["def", "ghi"], "xyz": ["123", "456"]}

        self.assertEqual(dict_, extract_key_value_pairs(tree))
