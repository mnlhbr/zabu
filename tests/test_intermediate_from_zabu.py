import unittest
import typing
from zabu.intermediate import Type
from zabu.intermediate.from_zabu import convert as fzc
from zabu.intermediate.paragraph import Paragraph
from zabu.intermediate.heading import Heading
from zabu.intermediate.word import Word
from zabu.intermediate.space import Space, EOL
from zabu.intermediate.internal_link import InternalLink
from zabu.intermediate.external_link import ExternalLink
from zabu.intermediate.unordered_list import UnorderedList


class _Node(typing.NamedTuple):
    name: str
    content: typing.Union[list, str]


def _lex(lexeme):
    return _Node("lexeme", lexeme)


def _nlex(name, lexeme):
    return _Node(name, [_lex(lexeme)])


def _space(lexeme=" "):
    return _nlex("space", lexeme)


def _eol(lexeme="\n"):
    return _nlex("eol", lexeme)


def _word(lexeme):
    return _nlex("word", lexeme)


class TestTypes(unittest.TestCase):

    def test_paragraph_returns_its_type(self):
        self.assertEqual(Type.PARAGRAPH, Paragraph([]).type())

    def test_word_returns_its_type(self):
        self.assertEqual(Type.WORD, Word("").type())

    def test_eol_returns_its_type(self):
        self.assertEqual(Type.EOL, EOL("").type())

    def test_space_returns_its_type(self):
        self.assertEqual(Type.SPACE, Space("").type())

    def test_heading_returns_its_type(self):
        self.assertEqual(Type.HEADING, Heading(Heading.Level.L1, "").type())

    def test_internal_link_returns_its_type(self):
        self.assertEqual(Type.INTERNAL_LINK, InternalLink("").type())

    def test_external_link_returns_its_type(self):
        self.assertEqual(Type.EXTERNAL_LINK, ExternalLink("").type())

    def test_unordered_list_returns_its_type(self):
        self.assertEqual(Type.UNORDERED_LIST, UnorderedList([]).type())


class TestCreateParagraph(unittest.TestCase):

    def test_create_paragraph_with_single_word(self):
        tree = [_Node("paragraph", [_nlex("word", "abc"), _nlex("eol", "\n")])]
        im = [Paragraph([Word("abc"), EOL("\n")])]
        self.assertEqual(im, fzc(tree))

    def test_create_paragraph_with_multiple_words(self):
        tree = [_Node("paragraph", [_nlex("word", "abc"), _nlex("space", " "),
                                    _nlex("word", "def"), _nlex("eol", "\n")])]
        im = [Paragraph([Word("abc"), Space(" "), Word("def"), EOL("\n")])]
        self.assertEqual(im, fzc(tree))

    def test_keep_space_lexeme(self):
        tree = [_Node("paragraph", [_nlex("space", "    ")])]
        im = [Paragraph([Space("    ")])]
        self.assertEqual(im, fzc(tree))

    def test_keep_eol_lexeme(self):
        tree = [_Node("paragraph", [_nlex("eol", "\r\n")])]
        im = [Paragraph([EOL("\r\n")])]
        self.assertEqual(im, fzc(tree))


class TestCreateHeading(unittest.TestCase):

    @staticmethod
    def heading(level: int, content: list):
        name = f"heading{level}"

        return _Node(name, [_lex("="), _space(),
                            _Node("heading_content", content),
                            _eol()])

    def test_create_one_word_heading(self):
        tree = [self.heading(1, [_word("xyz")])]

        im = [Heading(Heading.Level.L1, [Word("xyz")])]

        self.assertEqual(im, fzc(tree))

    def test_create_longer_heading(self):
        tree = [self.heading(1, [_word("abc"), _space(), _word("def")])]

        im = [Heading(Heading.Level.L1, [
            Word("abc"), Space(" "), Word("def")])]

        self.assertEqual(im, fzc(tree))

    def test_create_multiple_headings_with_different_levels(self):
        tree = [self.heading(1, [_word("abc")]),
                self.heading(2, [_word("def")]),
                self.heading(3, [_word("ghi")])]

        im = [Heading(Heading.Level.L1, [Word("abc")]),
              Heading(Heading.Level.L2, [Word("def")]),
              Heading(Heading.Level.L3, [Word("ghi")])]

        self.assertEqual(im, fzc(tree))


class TestCreateInternalLink(unittest.TestCase):

    @staticmethod
    def paragraph(content):
        return _Node("paragraph", content)

    @staticmethod
    def link(url, link_text=None):
        if link_text is None:
            description = []
        else:
            description = [_Node("link_description", [
                _lex("|"),
                _Node("link_text", link_text)])]

        return _Node("internal_link", [
            _lex("["),
            _Node("internal_link_url", [_lex(url)]),
            *description,
            _lex("]")])

    def test_create_simple_link_without_description(self):
        tree = [self.paragraph([self.link("link")])]

        im = [Paragraph([InternalLink("link", [Word("link")])])]

        self.assertEqual(im, fzc(tree))

    def test_create_longer_link_without_description(self):
        tree = [self.paragraph([self.link("..:very:long:link:")])]

        im = [Paragraph([InternalLink("..:very:long:link:",
                                      [Word("..:very:long:link:")])])]

        self.assertEqual(im, fzc(tree))

    def test_create_link_with_simple_description(self):
        tree = [self.paragraph([self.link("link", [_word("description")])])]

        im = [Paragraph([InternalLink("link", [Word("description")])])]

        self.assertEqual(im, fzc(tree))

    def test_create_link_with_longer_description(self):
        tree = [self.paragraph([self.link("linky", [
            _word("link"), _space(), _word("desc")])])]

        im = [Paragraph([InternalLink("linky", [
            Word("link"), Space(" "), Word("desc")])])]

        self.assertEqual(im, fzc(tree))


class TestCreateExternalLink(unittest.TestCase):

    @staticmethod
    def paragraph(content):
        return _Node("paragraph", content)

    @staticmethod
    def link(url, link_text=None):
        if link_text is None:
            description = []
        else:
            description = [_Node("link_description", [
                _lex("|"),
                _Node("link_text", link_text)])]

        return _Node("external_link", [
            _lex("[[]"),
            _Node("external_link_url", [_lex(url)]),
            *description,
            _lex("]}")])

    def test_create_simple_link_without_description(self):
        tree = [self.paragraph([self.link("example.com")])]

        im = [Paragraph([ExternalLink("example.com", [Word("example.com")])])]

        self.assertEqual(im, fzc(tree))

    def test_create_longer_link_without_description(self):
        link = "https://www.example.com/example.html"
        tree = [self.paragraph([self.link(link)])]

        im = [Paragraph([ExternalLink(link,
                                      [Word(link)])])]

        self.assertEqual(im, fzc(tree))

    def test_create_link_with_simple_description(self):
        tree = [self.paragraph([self.link("www.example.com",
                                          [_word("example")])])]

        im = [Paragraph([ExternalLink("www.example.com",
                                      [Word("example")])])]

        self.assertEqual(im, fzc(tree))

    def test_create_link_with_longer_description(self):
        tree = [self.paragraph([self.link("example.com", [
            _word("example"), _space(), _word("page")])])]

        im = [Paragraph([ExternalLink("example.com", [
            Word("example"), Space(" "), Word("page")])])]

        self.assertEqual(im, fzc(tree))


class TestCreateUnorderedList(unittest.TestCase):

    @staticmethod
    def ult(contents):
        items = []

        for content in contents:
            items.append(_Node("unordered_list_item", content))

        return _Node("unordered_list", items)

    @staticmethod
    def uli(contents):
        items = []

        for content in contents:
            items.append(UnorderedList.ListItem(content))

        return UnorderedList(items)

    def test_create_list_with_one_item(self):
        tree = [self.ult([[_lex("  * "), _word("abc"), _eol()]])]

        im = [self.uli([[Word("abc"), EOL("\n")]])]

        self.assertEqual(im, fzc(tree))

    def test_create_list_with_multiline_item(self):
        tree = [self.ult([[_lex("  * "), _word("first"), _eol(),
                           _lex("    "), _word("second"), _eol()]])]

        im = [self.uli([[Word("first"), EOL("\n"),
                         Word("second"), EOL("\n")]])]

        self.assertEqual(im, fzc(tree))

    def test_create_list_with_multiple_items(self):
        tree = [self.ult([[_lex("  * "), _word("first"), _eol()],
                          [_lex("  * "), _word("second"), _eol()]])]

        im = [self.uli([[Word("first"), EOL("\n")],
                        [Word("second"), EOL("\n")]])]

        self.assertEqual(im, fzc(tree))

    def test_create_simple_nested_list(self):
        tree = [self.ult([[
            _lex("  * "), _word("fruits"), _eol(),
            self.ult([[_lex("    * "), _word("apple"), _eol()],
                      [_lex("    * "), _word("banana"), _eol()]])]])]

        im = [self.uli([[Word("fruits"), EOL("\n"),
                         self.uli([[Word("apple"), EOL("\n")],
                                   [Word("banana"), EOL("\n")]])]])]

        self.assertEqual(im, fzc(tree))

    def test_create_longer_nested_list(self):
        tree = [self.ult([
            [_lex("  * "), _word("cars"), _eol(),
             self.ult([[_lex("    * "), _word("fast"), _eol()],
                       [_lex("    * "), _word("slow"), _eol()]])],
            [_lex("  * "), _word("numbers"), _eol(),
             self.ult([[_lex("    * "), _word("42"), _eol()],
                       [_lex("    * "), _word("6.28"), _eol()]])]])]

        im = [self.uli([[Word("cars"), EOL("\n"),
                         self.uli([[Word("fast"), EOL("\n")],
                                   [Word("slow"), EOL("\n")]])],
                        [Word("numbers"), EOL("\n"),
                         self.uli([[Word("42"), EOL("\n")],
                                   [Word("6.28"), EOL("\n")]])]])]

        self.assertEqual(im, fzc(tree))
