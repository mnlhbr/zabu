import unittest
from unittest.mock import MagicMock
import typing
from zabu.grammar import convert_from_infix_to_postfix
from zabu.grammar import create_parser
from zabu.grammar import Rule
from zabu.grammar import create_rules
from zabu.grammar import extract_grammar_parts
from zabu.lexer import Token
from zabu.util import Iterator
from zabu.lexer import LexerIterator


class Node(typing.NamedTuple):
    name: str
    content: str


class TestRule(unittest.TestCase):

    def test_returns_empty_node_name_when_invisible(self):
        rule = Rule("n", False, [], "")
        self.assertEqual("", rule.get_node_name())

    def test_returns_empty_node_name_when_invisible_even_with_given_name(self):
        rule = Rule("n", False, [], "x")
        self.assertEqual("", rule.get_node_name())

    def test_returns_rule_name_when_visible(self):
        rule = Rule("n", True, [])
        self.assertEqual("n", rule.get_node_name())

    def test_returns_node_name_when_visible(self):
        rule = Rule("n", True, [], "x")
        self.assertEqual("x", rule.get_node_name())


def _string_to_nodes(string):
    s2n = {"0": Node("num", "42"),
           "1": Node("num", "314"),
           "2": Node("num", "628"),
           "v": Node("var", "global"),
           "+": Node("plus", "+"),
           "*": Node("mult", "*"),
           "x": Node("xmul", "x"),
           "(": Node("lparen", "("),
           ")": Node("rparen", ")"),
           ".": Node("point", "."),
           "$": Node("mny", "$"),
           "?": Node("qm", "?"),
           "!": Node("em", "!")}

    return [s2n[char] for char in string]


class TestConvertFromInfixToPosfix(unittest.TestCase):

    def setUp(self):
        self.operands = set(("num", "var"))
        self.operators = {"plus": 0, "mult": 1, "xmul": 1}

        self.in2post = lambda e: convert_from_infix_to_postfix(
            e, self.operands, self.operators, _string_to_nodes("*")[0])

    def test_empty_expression_returns_empty_expression(self):
        self.assertEqual([], self.in2post([]))

    def test_single_operand_is_returned(self):
        self.assertEqual(_string_to_nodes("0"),
                         self.in2post(_string_to_nodes("0")))

    def test_converts_simple_expression(self):
        infix = _string_to_nodes("0+1")
        postfix = _string_to_nodes("01+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_converts_longer_simple_expression(self):
        infix = _string_to_nodes("0+1+2")
        postfix = _string_to_nodes("012++")

        self.assertEqual(postfix, self.in2post(infix))

    def test_converts_expression_with_different_operands(self):
        infix = _string_to_nodes("0+v+2")
        postfix = _string_to_nodes("0v2++")

        self.assertEqual(postfix, self.in2post(infix))

    def test_converts_expression_with_different_operators(self):
        infix = _string_to_nodes("0+1*2")
        postfix = _string_to_nodes("012*+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_handles_operators_with_different_precedence(self):
        infix = _string_to_nodes("0*1+2")
        postfix = _string_to_nodes("01*2+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_handles_more_operators_with_different_precedence(self):
        infix = _string_to_nodes("0*1*2+0")
        postfix = _string_to_nodes("012**0+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_uses_default_operator(self):
        infix = _string_to_nodes("01")
        postfix = _string_to_nodes("01*")

        self.assertEqual(postfix, self.in2post(infix))

    def test_combines_default_operator_with_other_operator(self):
        infix = _string_to_nodes("01+20")
        postfix = _string_to_nodes("01*20*+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_combines_default_operator_with_lower_precedence(self):
        infix = _string_to_nodes("01*20")
        postfix = _string_to_nodes("012*0++")

        self.assertEqual(postfix, convert_from_infix_to_postfix(
            infix, self.operands, self.operators, _string_to_nodes("+")[0]))

    def test_different_operators_with_same_precedence_are_not_combined(self):
        infix = _string_to_nodes("0*1x2")
        postfix = _string_to_nodes("01*2x")

        self.assertEqual(postfix, self.in2post(infix))


class TestConvertFromInfixToPostfixWithParenthesis(unittest.TestCase):

    def setUp(self):
        self.operands = set(("num", "var"))
        self.operators = {"plus": 0, "mult": 1}

        self.in2post = lambda e: convert_from_infix_to_postfix(
            e, self.operands, self.operators, _string_to_nodes("*")[0],
            left_paren="lparen", right_paren="rparen")

    def test_returns_single_operand_in_parenthesis(self):
        infix = _string_to_nodes("(1)")
        postfix = _string_to_nodes("1")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_simple_expression_in_parenthesis(self):
        infix = _string_to_nodes("(1+2)")
        postfix = _string_to_nodes("12+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_converts_operation_after_parenthesis(self):
        infix = _string_to_nodes("(1+2)*v")
        postfix = _string_to_nodes("12+v*")

        self.assertEqual(postfix, self.in2post(infix))

    def test_converts_operation_before_parenthesis(self):
        infix = _string_to_nodes("1*(2+v)")
        postfix = _string_to_nodes("12v+*")

        self.assertEqual(postfix, self.in2post(infix))

    def test_converts_two_operations_in_parenthesis(self):
        infix = _string_to_nodes("(0+2)*(v+1)")
        postfix = _string_to_nodes("02+v1+*")

        self.assertEqual(postfix, self.in2post(infix))

    def test_converts_expressions_in_multiple_parenthesis(self):
        infix = _string_to_nodes("0+(((1*2)+v))*1")
        postfix = _string_to_nodes("012*v+1*+")

        self.assertEqual(postfix, self.in2post(infix))


class TestConvertFromInfixToPostfixWithUnaryOperators(unittest.TestCase):

    def setUp(self):
        self.operands = set(("num", "var"))
        self.operators = {"plus": 0, "mult": 1}
        pre_operators = set(("point", "mny"))
        post_operators = set(("qm", "em"))

        self.in2post = lambda expression: convert_from_infix_to_postfix(
            expression, self.operands, self.operators, None,
            left_paren="lparen", right_paren="rparen",
            pre_operators=pre_operators, post_operators=post_operators)

    def test_returns_single_post_op_after_expression(self):
        infix = _string_to_nodes("v?")
        postfix = _string_to_nodes("v?")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_post_op_in_expression(self):
        infix = _string_to_nodes("2!+1?")
        postfix = _string_to_nodes("2!1?+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_post_op_in_longer_expression(self):
        infix = _string_to_nodes("0+1?*2!+v?")
        postfix = _string_to_nodes("01?2!*v?++")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_post_op_with_parenthesis(self):
        infix = _string_to_nodes("0?*(1?+v)!")
        postfix = _string_to_nodes("0?1?v+!*")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_multiple_post_operators_in_expression(self):
        infix = _string_to_nodes("0??+1?!*2!!")
        postfix = _string_to_nodes("0??1?!2!!*+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_single_pre_op_before_expression(self):
        infix = _string_to_nodes(".1")
        postfix = _string_to_nodes("1.")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_pre_op_in_expression(self):
        infix = _string_to_nodes("$v+.2")
        postfix = _string_to_nodes("v$2.+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_pre_op_in_longer_expression(self):
        infix = _string_to_nodes("0+.1*$2+.v")
        postfix = _string_to_nodes("01.2$*v.++")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_pre_op_with_parenthesis(self):
        infix = _string_to_nodes("$0*.($2+v)")
        postfix = _string_to_nodes("0$2$v+.*")

        self.assertEqual(postfix, self.in2post(infix))

    def test_returns_multiple_pre_operators_in_expression(self):
        infix = _string_to_nodes("$$0+.$1*..2")
        postfix = _string_to_nodes("0$$1$.2..*+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_handles_pre_and_post_operators(self):
        infix = _string_to_nodes(".$1?+.v?!")
        postfix = _string_to_nodes("1?$.v?!.+")

        self.assertEqual(postfix, self.in2post(infix))

    def test_handles_pre_and_post_operators_with_parenthesis(self):
        infix = _string_to_nodes("$.(.v+2?)!")
        postfix = _string_to_nodes("v.2?+!.$")

        self.assertEqual(postfix, self.in2post(infix))


def _string_to_grammar_terminal_content(string):
    return [Node("lexeme", "'"),
            Node("lexeme", string),
            Node("lexeme", "'")]


def _string_to_grammar_nodes(string):
    to_terminal = _string_to_grammar_terminal_content

    s2n = {**{str(i): Node("terminal", to_terminal(str(i))) for i in range(8)},
           "&": Node("and", [Node("lexeme", "&")]),
           "|": Node("or", [Node("lexeme", "|")]),
           "?": Node("repetition01", [Node("lexeme", "?")]),
           "*": Node("repetition0inf", [Node("lexeme", "*")]),
           "+": Node("repetition1inf", [Node("lexeme", "+")]),
           "s": Node("nonterminal", [Node("lexeme", "s")]),
           "a": Node("nonterminal", [Node("lexeme", "a")]),
           "b": Node("nonterminal", [Node("lexeme", "b")]),
           "c": Node("nonterminal", [Node("lexeme", "c")]),
           "d": Node("nonterminal", [Node("lexeme", "d")]),
           "m": Node("change_lexer", [None, Node("lexer_name",
                                                 [Node("lexeme", "m")])]),
           "n": Node("change_lexer", [None, Node("lexer_name",
                                                 [Node("lexeme", "n")])]),
           "p": Node("change_lexer", [None, Node("lexer_name",
                                                 [Node("lexeme", " ")])])}

    return [s2n[char] for char in string]


def _string_to_tokens(string):
    s2t = {"0": Token("0", "0"),
           "1": Token("1", "1"),
           "2": Token("2", "2"),
           "3": Token("3", "3")}

    return [s2t[char] for char in string]


def _string_to_tree_nodes(string):
    return _string_to_tree_nodes_helper(string)[0]


def _string_to_tree_nodes_helper(string):
    s2tn = {"0": Node("lexeme", "0"),
            "1": Node("lexeme", "1"),
            "2": Node("lexeme", "2"),
            "3": Node("lexeme", "3")}

    nonterminal = {"s", "a", "b", "c", "d"}

    tree = []
    i = 0

    while i < len(string):
        if string[i] in nonterminal:
            content, j = _string_to_tree_nodes_helper(string[i + 1:])
            tree.append(Node(string[i], content))
            i += j + 1  # nodes plus right parenthesis
        elif string[i] in s2tn:
            tree.append(s2tn[string[i]])
        elif string[i] == ")":
            break
        i += 1

    return tree, i


def _create_test_data(rule_list, token_string, tree_string):
    rule_list = [e if isinstance(e, list) else [e] for e in rule_list]
    rules = {r[0].name: r for r in rule_list}
    # create_parser is the function to test
    parser, _ = create_parser(rule_list[0][0].name, rules)
    # the input to test the created parser
    tokens = _string_to_tokens(token_string)
    # the output the parser should create
    tree = _string_to_tree_nodes(tree_string)

    return parser, Iterator(tokens), tree


class TestCreateParserProduction(unittest.TestCase):

    def test_parse_simple_terminal(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0"))]
        parser, tokens, tree = _create_test_data(rule, "0", "0")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_two_terminals(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("01&"))]
        parser, tokens, tree = _create_test_data(rule, "01", "01")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_visible_production_with_one_terminal(self):
        rule = [Rule("s", True, _string_to_grammar_nodes("0"))]
        parser, tokens, tree = _create_test_data(rule, "0", "s(0)")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_visible_production_with_multiple_terminals(self):
        rule = [Rule("s", True, _string_to_grammar_nodes("010&&"))]
        parser, tokens, tree = _create_test_data(rule, "010", "s(010)")
        self.assertEqual(tree, parser.parse(tokens)[0])


class TestCreateParserOption(unittest.TestCase):

    def test_parse_one_of_two_terminals(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("01|"))]
        parser, tokens, tree = _create_test_data(rule, "0", "0")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_one_of_multiple_terminals(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0123|||"))]
        parser, tokens, tree = _create_test_data(rule, "2", "2")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_two_of_multiple_terminals(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("01|23|&"))]
        parser, tokens, tree = _create_test_data(rule, "12", "12")
        self.assertEqual(tree, parser.parse(tokens)[0])


class TestCreateParserRepetition(unittest.TestCase):

    def test_parse_missing_optional_terminal(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0?"))]
        parser, tokens, tree = _create_test_data(rule, "", "")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_existing_optional_terminal(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0?"))]
        parser, tokens, tree = _create_test_data(rule, "0", "0")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_missing_terminal_repetition(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0*"))]
        parser, tokens, tree = _create_test_data(rule, "", "")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_one_terminal_of_0_inf_repetition(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0*"))]
        parser, tokens, tree = _create_test_data(rule, "0", "0")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_multiple_terminals_of_0_inf_repetition(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0*"))]
        parser, tokens, tree = _create_test_data(rule, "000", "000")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_one_terminal_of_1_inf_repetition(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0+"))]
        parser, tokens, tree = _create_test_data(rule, "0", "0")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_multiple_terminals_of_1_inf_repetition(self):
        rule = [Rule("s", False, _string_to_grammar_nodes("0+"))]
        parser, tokens, tree = _create_test_data(rule, "000", "000")
        self.assertEqual(tree, parser.parse(tokens)[0])


class TestCreateParserNonterminal(unittest.TestCase):

    def test_parse_other_nonterminal(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("a")),
                 Rule("a", False, _string_to_grammar_nodes("0"))]
        parser, tokens, tree = _create_test_data(rules, "0", "0")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_visible_nonterminal(self):
        rules = [Rule("s", True, _string_to_grammar_nodes("a")),
                 Rule("a", True, _string_to_grammar_nodes("0"))]
        parser, tokens, tree = _create_test_data(rules, "0", "s(a(0))")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_multiple_nonterminals(self):
        rules = [Rule("s", True, _string_to_grammar_nodes("aba")),
                 Rule("a", False, _string_to_grammar_nodes("0")),
                 Rule("b", True, _string_to_grammar_nodes("1"))]
        parser, tokens, tree = _create_test_data(rules, "010", "s(0b(1)0)")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_multiple_invisible_nonterminals(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("aba")),
                 Rule("a", False, _string_to_grammar_nodes("0")),
                 Rule("b", False, _string_to_grammar_nodes("1"))]
        parser, tokens, tree = _create_test_data(rules, "010", "010)")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_multiple_visible_nonterminals(self):
        rules = [Rule("s", True, _string_to_grammar_nodes("aba")),
                 Rule("a", True, _string_to_grammar_nodes("0")),
                 Rule("b", True, _string_to_grammar_nodes("1"))]
        parser, tokens, tree = _create_test_data(rules, "010",
                                                 "s(a(0)b(1)a(0))")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_different_multiple_nonterminals(self):
        rules = [Rule("b", True, _string_to_grammar_nodes("sas")),
                 Rule("s", False, _string_to_grammar_nodes("0")),
                 Rule("a", True, _string_to_grammar_nodes("1"))]
        parser, tokens, tree = _create_test_data(rules, "010", "b(0a(1)0)")
        self.assertEqual(tree, parser.parse(tokens)[0])


class TestCreateParserNonterminalWithNodeName(unittest.TestCase):

    def test_parse_simple_nonterminal_with_node_name(self):
        rule = [Rule("s", True, _string_to_grammar_nodes("0"), "a")]
        parser, tokens, tree = _create_test_data(rule, "0", "a(0)")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_nt_with_multiple_rules_with_node_names(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("aa")),
                 [Rule("a", True, _string_to_grammar_nodes("0"), "b"),
                  Rule("a", True, _string_to_grammar_nodes("1"), "c")]]
        parser, tokens, tree = _create_test_data(rules, "01", "b(0)c(1)")
        self.assertEqual(tree, parser.parse(tokens)[0])


class TestCreateParserNonterminalWithMultipleRules(unittest.TestCase):

    def test_parse_nonterminal_with_multiple_invisible_rules(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("aa&")),
                 [Rule("a", False, _string_to_grammar_nodes("1")),
                  Rule("a", False, _string_to_grammar_nodes("0"))]]
        parser, tokens, tree = _create_test_data(rules, "10", "10")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_nt_with_multiple_rules_where_first_is_visible(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("aa&")),
                 [Rule("a", True, _string_to_grammar_nodes("1")),
                  Rule("a", False, _string_to_grammar_nodes("0"))]]
        parser, tokens, tree = _create_test_data(rules, "10", "a(1)0")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_nt_with_multiple_rules_where_last_is_visible(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("aa&")),
                 [Rule("a", False, _string_to_grammar_nodes("1")),
                  Rule("a", True, _string_to_grammar_nodes("0"))]]
        parser, tokens, tree = _create_test_data(rules, "10", "1a(0)")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_start_nonterminal_with_multiple_rules(self):
        rules = [[Rule("s", True, _string_to_grammar_nodes("0")),
                  Rule("s", True, _string_to_grammar_nodes("a"))],
                 Rule("a", False, _string_to_grammar_nodes("1"))]
        parser, tokens, tree = _create_test_data(rules, "1", "s(1)")
        self.assertEqual(tree, parser.parse(tokens)[0])


class TestCreateParserNonterminalWithSelfReference(unittest.TestCase):

    def test_parse_start_nonterminal_with_self_reference(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("1s&1|"))]

        for string in ["1", "111"]:
            with self.subTest(string=string):
                parser, tokens, tree = _create_test_data(rules, string, string)
                self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_nonterminal_with_self_reference(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("a0&")),
                 Rule("a", False, _string_to_grammar_nodes("1a&1|"))]

        for string in ["10", "110", "1110"]:
            with self.subTest(string=string):
                parser, tokens, tree = _create_test_data(rules, string, string)
                self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_nonterminals_with_circular_reference(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("1a&1|")),
                 Rule("a", False, _string_to_grammar_nodes("0s&0|"))]

        for string in ["10", "101", "1010", "10101"]:
            with self.subTest(string=string):
                parser, tokens, tree = _create_test_data(rules, string, string)
                self.assertEqual(tree, parser.parse(tokens)[0])


class TestCreateParser(unittest.TestCase):

    def test_parse_option_of_productions(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("aa&bb&|")),
                 Rule("a", False, _string_to_grammar_nodes("0")),
                 Rule("b", False, _string_to_grammar_nodes("1"))]
        parser, tokens, tree = _create_test_data(rules, "11", "11")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_parse_option_of_longer_productions(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("aaa&&bbb&&|")),
                 Rule("a", False, _string_to_grammar_nodes("0")),
                 Rule("b", False, _string_to_grammar_nodes("1"))]
        parser, tokens, tree = _create_test_data(rules, "111", "111")
        self.assertEqual(tree, parser.parse(tokens)[0])

    def test_combine_repetition_and_option(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("ab|+")),
                 Rule("a", False, _string_to_grammar_nodes("0")),
                 Rule("b", False, _string_to_grammar_nodes("1"))]
        parser, tokens, tree = _create_test_data(rules, "110", "110")
        self.assertEqual(tree, parser.parse(tokens)[0])


class TestCreateParserPatterns(unittest.TestCase):

    def test_create_single_pattern(self):
        rule = {"s": [Rule("s", False, _string_to_grammar_nodes("1"))]}
        _, patterns = create_parser("s", rule)
        self.assertEqual(["1"], patterns)

    def test_create_multiple_patterns(self):
        rules = {"s": [Rule("s", False, _string_to_grammar_nodes("a0&"))],
                 "a": [Rule("a", True, _string_to_grammar_nodes("1"))]}
        _, patterns = create_parser("s", rules)
        self.assertEqual(["0", "1"], list(sorted(patterns)))

    def test_create_patterns_with_longer_grammer(self):
        rules = {"s": [Rule("s", True, _string_to_grammar_nodes("a2b&&"))],
                 "a": [Rule("a", False, _string_to_grammar_nodes("01|1&"))],
                 "b": [Rule("b", False, _string_to_grammar_nodes("3+"))]}
        _, patterns = create_parser("s", rules)
        self.assertEqual([str(i) for i in range(4)], list(sorted(patterns)))


class TestCreateParserChangeLexer(unittest.TestCase):

    def setUp(self):
        lxr = {}

        for i, name in enumerate(("l", "m", "n")):
            lxr[name] = MagicMock()
            lxr[name].return_value = _string_to_tokens(str(i))[0]
            lxr[name].copy.side_effect = lambda name=name: lxr[name]

        self.lexer_iterator = LexerIterator(lxr, "l", MagicMock())

    def test_change_lexer_after_first_token(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("0m1&&"))]
        parser, _, tree = _create_test_data(rules, "01", "01")

        self.assertEqual(tree, parser.parse(self.lexer_iterator)[0])

    def test_change_lexer_and_then_change_it_back(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("0m1p0&&&&"))]
        parser, _, tree = _create_test_data(rules, "010", "010")

        self.assertEqual(tree, parser.parse(self.lexer_iterator)[0])

    def test_change_different_lexers(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("0m1pn2p0"))]
        parser, _, tree = _create_test_data(rules, "0120", "0120")

        self.assertEqual(tree, parser.parse(self.lexer_iterator)[0])

    def test_change_lexer_multiple_times(self):
        rules = [Rule("s", False, _string_to_grammar_nodes("0m1n2p1p0"))]
        parser, _, tree = _create_test_data(rules, "01210", "01210")

        self.assertEqual(tree, parser.parse(self.lexer_iterator)[0])


class TestCreateParserCollectLexerPatternStrings(unittest.TestCase):

    @staticmethod
    def create_rules(data):
        rules = {}
        for name, *strings in data:
            rules[name] = [Rule(name, False, _string_to_grammar_nodes(string))
                           for string in strings]

        return rules

    @staticmethod
    def collect_patterns(rules):
        pattern_strings = {None: set()}

        _, _ = create_parser("s", rules, pattern_strings)

        return pattern_strings

    def test_collect_pattern_of_main_parser(self):
        rules = self.create_rules([("s", "010")])
        pattern_strings = self.collect_patterns(rules)

        self.assertEqual(pattern_strings, {None: {"0", "1"}})

    def test_collect_pattern_of_multiple_rules(self):
        rules = self.create_rules([("s", "a"),
                                  ("a", "02", "34")])
        pattern_strings = self.collect_patterns(rules)

        self.assertEqual(pattern_strings, {None: {"0", "2", "3", "4"}})

    def test_collect_pattern_of_other_lexer(self):
        rules = self.create_rules([("s", "0m1p0")])
        pattern_strings = self.collect_patterns(rules)

        self.assertEqual(pattern_strings, {None: {"0"}, "m": {"1"}})

    def test_collect_pattern_of_multiple_lexer_in_one_rule(self):
        rules = self.create_rules([("s", "0m1n23p4p5")])
        pattern_strings = self.collect_patterns(rules)

        self.assertEqual(pattern_strings, {None: {"0", "5"},
                                           "m": {"1", "4"},
                                           "n": {"2", "3"}})

    def test_collect_pattern_of_different_lexer_in_several_rules(self):
        rules = self.create_rules([("s", "0ab1"),
                                   ("a", "m2p"),
                                   ("b", "3n4p")])
        pattern_strings = self.collect_patterns(rules)

        self.assertEqual(pattern_strings, {None: {"0", "1", "3"},
                                           "m": {"2"},
                                           "n": {"4"}})

    def test_collect_pattern_with_lexer_change_over_multiple_rules(self):
        rules = self.create_rules([("s", "0a"),
                                   ("a", "1m2b1"),
                                   ("b", "3p")])
        pattern_strings = self.collect_patterns(rules)

        self.assertEqual(pattern_strings, {None: {"0", "1"},
                                           "m": {"2", "3"}})


def _rn(name, content=""):
    return Node(name, [Node("lexeme", content if content else name)])


def _rns(name, visible, rhs, node_name=""):
    lhs = Node("lhs", ([] if visible else [Node("invisible", "")]) +
               [_rn("name", name)] +
               ([_rn("node_name", node_name)] if node_name else []))
    seperator = Node("lexeme", ":=")
    rhs = [Node("rhs", rhs)]

    return Node("rule", [lhs, seperator] + rhs)


class TestCreateRules(unittest.TestCase):

    def test_empty_tree_creates_no_rules(self):
        self.assertEqual({}, create_rules([])[1])

    def test_empty_tree_returns_no_start_name(self):
        self.assertIsNone(create_rules([])[0])

    def test_ignores_empty_lines(self):
        nodes = [Node("eol", ""),
                 _rns("ra", True, [_rn("a")]),
                 Node("eol", ""),
                 _rns("rb", True, [_rn("b")]),
                 Node("eol", "")]

        rules = {"ra": [Rule("ra", True, [_rn("a")])],
                 "rb": [Rule("rb", True, [_rn("b")])]}

        self.assertEqual("ra", create_rules(nodes)[0])
        self.assertEqual(rules, create_rules(nodes)[1])

    def test_ignores_comments(self):
        nodes = [Node("comment", ""),
                 Node("comment", ""),
                 _rns("ra", True, [_rn("a")]),
                 Node("comment", ""),
                 Node("comment", ""),
                 Node("comment", ""),
                 _rns("rb", True, [_rn("b")]),
                 Node("comment", "")]

        rules = {"ra": [Rule("ra", True, [_rn("a")])],
                 "rb": [Rule("rb", True, [_rn("b")])]}

        self.assertEqual("ra", create_rules(nodes)[0])
        self.assertEqual(rules, create_rules(nodes)[1])

    def test_create_simple_visible_rule_with_one_element(self):
        rhs = [_rn("a")]
        nodes = [_rns("r", True, rhs)]
        rule = {"r": [Rule("r", True, rhs)]}

        self.assertEqual(rule, create_rules(nodes)[1])

    def test_create_simple_visible_rule_with_two_elements(self):
        rhs = [_rn("a"), _rn("b")]
        nodes = [_rns("r", True, rhs)]
        rule = {"r": [Rule("r", True, rhs)]}

        self.assertEqual(rule, create_rules(nodes)[1])

    def test_create_simple_invisible_rule_with_two_elements(self):
        rhs = [_rn("a"), _rn("b")]
        nodes = [_rns("r", False, rhs)]
        rule = {"r": [Rule("r", False, rhs)]}

        self.assertEqual(rule, create_rules(nodes)[1])

    def test_create_multiple_simple_rules(self):
        nodes = [_rns("ra", True, [_rn("a")]),
                 _rns("rb", False, [_rn("b")])]
        rules = {"ra": [Rule("ra", True, [_rn("a")])],
                 "rb": [Rule("rb", False, [_rn("b")])]}

        self.assertEqual(rules, create_rules(nodes)[1])

    def test_apply_identity_transform_function_to_simple_rhs(self):
        rhs = [_rn("a")]
        nodes = [_rns("r", True, rhs)]
        rule = {"r": [Rule("r", True, rhs)]}

        self.assertEqual(rule, create_rules(nodes, lambda rhs: rhs)[1])

    def test_apply_transform_function_to_simple_rhs(self):
        rhs = [_rn("a")]
        nodes = [_rns("r", True, rhs)]
        rule = {"r": [Rule("r", True, 1)]}

        self.assertEqual(rule, create_rules(nodes, len)[1])

    def test_apply_transform_function_to_multiple_rules(self):
        nodes = [_rns("ra", True, [_rn("a"), _rn("b")]),
                 _rns("rb", False, [_rn("b")])]
        rules = {"ra": [Rule("ra", True, 2)],
                 "rb": [Rule("rb", False, 1)]}

        self.assertEqual(rules, create_rules(nodes, len)[1])

    def test_returns_start_name_of_only_rule(self):
        nodes = [_rns("r", True, [_rn("a")])]

        self.assertEqual("r", create_rules(nodes)[0])

    def test_returns_start_name_when_there_are_multiple_rules(self):
        nodes = [_rns("s", True, [_rn("a")]),
                 _rns("m", True, [_rn("a")])]

        self.assertEqual("s", create_rules(nodes)[0])

    def test_create_rules_with_same_name(self):
        nodes = [_rns("r", True, [_rn("a")]),
                 _rns("r", False, [_rn("b")])]
        rules = {"r": [Rule("r", True, [_rn("a")]),
                       Rule("r", False, [_rn("b")])]}

        self.assertEqual(rules, create_rules(nodes)[1])

    def test_create_multiple_rules_with_same_name(self):
        nodes = [_rns("r", True, [_rn("a")]),
                 _rns("x", False, [_rn("b")]),
                 _rns("r", False, [_rn("c")]),
                 _rns("o", True, [_rn("o")]),
                 _rns("x", False, [_rn("d")])]
        rules = {"r": [Rule("r", True, [_rn("a")]),
                       Rule("r", False, [_rn("c")])],
                 "x": [Rule("x", False, [_rn("b")]),
                       Rule("x", False, [_rn("d")])],
                 "o": [Rule("o", True, [_rn("o")])]}

        self.assertEqual(rules, create_rules(nodes)[1])

    def test_create_rule_with_given_node_name(self):
        nodes = [_rns("r", False, [_rn("a")], "n")]
        rules = {"r": [Rule("r", False, [_rn("a")], "n")]}

        self.assertEqual(rules, create_rules(nodes)[1])


class TestExtractGrammarParts(unittest.TestCase):

    def setUp(self):
        self.config = Node("config", "cfg")
        self.rules = Node("rules", ["rl0", "rl1"])
        self.lexer0 = Node("lexer", "lxr0")
        self.lexer1 = Node("lexer", "lxr1")
        self.delimiter = Node("delimiter", "dlmtr")

    def test_extracts_only_rules_of_simple_grammar(self):
        tree = [self.rules]

        parts = extract_grammar_parts(tree)

        self.assertFalse(parts.config)
        self.assertEqual(parts.rules, self.rules.content)
        self.assertFalse(parts.lexer)

    def test_extract_config_of_simple_grammar(self):
        tree = [self.config, self.delimiter, self.rules]

        parts = extract_grammar_parts(tree)

        self.assertEqual(parts.config, self.config.content)

    def test_extract_one_lexer_of_simple_grammar(self):
        tree = [self.rules, self.delimiter, self.lexer0]

        parts = extract_grammar_parts(tree)

        self.assertEqual(parts.lexer, [self.lexer0.content])

    def test_extract_more_lexer_of_simple_grammar(self):
        tree = [self.rules,
                self.delimiter, self.lexer0,
                self.delimiter, self.lexer1]

        parts = extract_grammar_parts(tree)

        self.assertEqual(parts.lexer, [self.lexer0.content,
                                       self.lexer1.content])

    def test_extract_all_grammar_parts(self):
        tree = [self.config, self.delimiter,
                self.rules,
                self.delimiter, self.lexer1,
                self.delimiter, self.lexer0]

        parts = extract_grammar_parts(tree)

        self.assertEqual(parts.config, self.config.content)
        self.assertEqual(parts.rules, self.rules.content)
        self.assertEqual(parts.lexer, [self.lexer1.content,
                                       self.lexer0.content])
