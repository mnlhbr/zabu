import unittest
from zabu.intermediate.indented_document import IndentedDocument
from zabu.intermediate.indented_document import NegativeIndentationError


class TestIndentedDocument(unittest.TestCase):

    def setUp(self):
        self.doc = IndentedDocument()

    def test_empty_document(self):
        self.assertEqual("", str(self.doc))

    def test_simple_string(self):
        self.doc.append("abc").append("def").append("xyz")
        self.assertEqual("abcdefxyz", str(self.doc))

    def test_append_line(self):
        self.doc.append_line("xxyyxx")
        self.assertEqual("xxyyxx\n", str(self.doc))

    def test_new_line(self):
        self.doc.append("abc").new_line().new_line().append("xyz").new_line()
        self.assertEqual("abc\n\nxyz\n", str(self.doc))

    def test_simple_indented_document(self):
        (self.doc
         .append_line("<x>").right()
         .append_line("yz")
         .left().append_line("</x>"))
        self.assertEqual("<x>\n  yz\n</x>\n", str(self.doc))

    def test_raises_when_indentation_would_get_negative(self):
        with self.assertRaises(NegativeIndentationError):
            self.doc.right().left().left()

    def test_indented_document(self):
        (self.doc
         .append_line("x").right()
         .append_line("y").right()
         .append_line("z")
         .left().append_line("y")
         .left().append_line("x"))

        self.assertEqual("x\n  y\n    z\n  y\nx\n", str(self.doc))
