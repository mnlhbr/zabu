import unittest
from unittest.mock import Mock
from zabu.lexer import Token
from zabu.util import Iterator
from zabu.parser import Terminal
from zabu.parser import Node
from zabu.parser import Production
from zabu.parser import Option
from zabu.parser import Repetition
from zabu.parser import Placeholder
from zabu.parser import LexerChanger


class TestTerminal(unittest.TestCase):

    def setUp(self):
        self.terminal = Terminal("t")

    def test_parses_empty_iterator_returns_none(self):
        iterator = Iterator([])

        self.assertIsNone(self.terminal.parse(iterator)[0])

    def test_parses_other_terminal_returns_none(self):
        iterator = Iterator([Token("n", "v")])

        self.assertIsNone(self.terminal.parse(iterator)[0])

    def test_parses_correct_terminal_returns_node(self):
        iterator = Iterator([Token("t", "v")])

        self.assertEqual([Node("lexeme", "v")],
                         self.terminal.parse(iterator)[0])

    def test_parses_correct_terminal_returns_advanced_iterator(self):
        tokens = [Token("t", "v"), Token("a", "b")]
        iterator = Iterator(tokens)

        _, iterator = self.terminal.parse(iterator.copy())

        self.assertIs(tokens[1], next(iterator))


class TestProduction(unittest.TestCase):

    def setUp(self):
        self.none = Mock()
        self.none.parse.return_value = None, None

        self.iter_zero = Mock()
        self.nodes = [Node("a", "b"), Node("c", "d")]

        self.iter_one = Mock()
        self.one = Mock()
        self.one.parse.return_value = [self.nodes[0]], self.iter_one

        self.iter_two = Mock()
        self.two = Mock()
        self.two.parse.return_value = [self.nodes[1]], self.iter_two

    def test_creating_an_empty_production_raises(self):
        with self.assertRaises(ValueError):
            Production([])

    def test_parses_mismatched_production_returns_none(self):
        production = Production([self.none])

        self.assertIsNone(production.parse(Mock())[0])

    def test_parses_simple_matching_production_returns_node(self):
        production = Production([self.one])

        tree, _ = production.parse(Mock())

        self.assertEqual([self.nodes[0]], tree)

    def test_parses_longer_matching_production_returns_nodes(self):
        production = Production([self.one, self.one])

        tree, _ = production.parse(Mock())

        self.assertEqual([self.nodes[0]] * 2, tree)

    def test_parses_other_matching_production_returns_nodes(self):
        production = Production([self.one, self.two])

        tree, _ = production.parse(Mock())

        self.assertEqual(self.nodes, tree)

    def test_parses_longer_mismatched_production_returns_none(self):
        production = Production([self.one, self.none, self.two])

        tree, _ = production.parse(Mock())

        self.assertIsNone(tree)

    def test_calls_production_with_given_tokens(self):
        production = Production([self.one])

        production.parse(self.iter_zero)

        self.one.parse.assert_called_with(self.iter_zero)

    def test_parses_one_production_returns_next_iterator(self):
        production = Production([self.one])

        _, iterator = production.parse(self.iter_zero)

        self.assertEqual(iterator, self.iter_one)

    def test_calls_multiple_production_with_returned_tokens(self):
        production = Production([self.one, self.two])

        production.parse(self.iter_zero)

        self.two.parse.assert_called_with(self.iter_one)

    def test_parses_longer_production_returns_last_iterator(self):
        production = Production([self.one, self.two])

        _, iterator = production.parse(self.iter_zero)

        self.assertEqual(self.iter_two, iterator)

    def test_parses_named_production_returns_named_node(self):
        production = Production([self.one, self.two], "abc")

        tree, _ = production.parse(Mock())

        self.assertEqual("abc", tree[0].name)

    def test_parses_named_production_returns_nodes_as_node_content(self):
        production = Production([self.one, self.two], "abc")

        tree, _ = production.parse(Mock())

        self.assertEqual(self.nodes, tree[0].content)

    def test_parses_named_production_with_node_name_returns_named_node(self):
        production = Production([self.one, self.two], "abc", "def")

        tree, _ = production.parse(Mock())

        self.assertEqual("def", tree[0].name)


class TestOption(unittest.TestCase):

    @staticmethod
    def parse(tokens, successful):
        next(tokens)

        if successful:
            return [], tokens

        return None, None

    def setUp(self):
        self.none = Mock()
        self.none.parse.return_value = None, None

        self.nodes = [Node("a", "b"), Node("c", "d")]

        self.one = Mock()
        self.one.parse.return_value = [self.nodes[0]], None

        self.two = Mock()
        self.two.parse.return_value = [self.nodes[1]], None

        self.parse_true = Mock()
        self.parse_true.parse = lambda tokens: self.parse(tokens, True)
        self.parse_false = Mock()
        self.parse_false.parse = lambda tokens: self.parse(tokens, False)
        self.tokens = Iterator("abc")

    def test_creating_an_empty_option_raises(self):
        with self.assertRaises(ValueError):
            Option([])

    def test_parses_mismatched_option_returns_none(self):
        option = Option([self.none])

        tree, _ = option.parse(Mock())

        self.assertIsNone(tree)

    def test_parses_matching_option_returns_node(self):
        option = Option([self.one])

        tree, _ = option.parse(Mock())

        self.assertEqual([self.nodes[0]], tree)

    def test_parses_first_matching_option_returns_first_node(self):
        option = Option([self.one, self.none])

        tree, _ = option.parse(Mock())

        self.assertEqual([self.nodes[0]], tree)

    def test_parses_later_matching_option_returns_its_node(self):
        option = Option([self.none, self.one])

        tree, _ = option.parse(Mock())

        self.assertEqual([self.nodes[0]], tree)

    def test_parses_multiple_mismatched_options_returns_none(self):
        option = Option([self.none] * 3)

        tree, _ = option.parse(Mock())

        self.assertIsNone(tree)

    def test_parses_multiple_matching_options_returns_first_match(self):
        option = Option([self.none, self.two, self.one])

        tree, _ = option.parse(Mock())

        self.assertEqual([self.nodes[1]], tree)

    def test_parses_first_option_returns_advanced_iterator(self):
        option = Option([self.parse_true, self.parse_false])

        _, iterator = option.parse(self.tokens)

        self.assertEqual("b", next(iterator))

    def test_parses_second_option_returns_advanced_iterator(self):
        option = Option([self.parse_false, self.parse_true])

        _, iterator = option.parse(self.tokens)

        self.assertEqual("b", next(iterator))


class TestRepetition(unittest.TestCase):

    @staticmethod
    def _parse(tokens):
        tree = next(tokens)

        if tree is not None and not isinstance(tree, list):
            tree = [tree]

        return tree, tokens

    def setUp(self):
        self.mock = Mock()
        self.mock.parse = TestRepetition._parse

        self.token = Token("k", "v")
        self.node = Node("k", "v")

    def test_min_number_less_than_zero_raises(self):
        with self.assertRaises(ValueError):
            Repetition(self.mock, -1)

    def test_parses_mismatched_optional_element_returns_empty_tree(self):
        tokens = Iterator([None])
        repetition = Repetition(self.mock, 0)

        tree, _ = repetition.parse(tokens)

        self.assertEqual([], tree)

    def test_parses_mismatched_optional_element_returns_unchanged_iter(self):
        tokens = Iterator([None])
        repetition = Repetition(self.mock, 0)

        _, iterator = repetition.parse(tokens)

        self.assertIsNone(next(iterator))

    def test_parses_one_optional_element_returns_one_element(self):
        tokens = Iterator([self.token, None])
        repetition = Repetition(self.mock, 0)

        tree, _ = repetition.parse(tokens)

        self.assertEqual([self.node], tree)

    def test_parses_one_optional_element_returns_advanced_iterator(self):
        tokens = Iterator([self.token, None])
        repetition = Repetition(self.mock, 0)

        _, iterator = repetition.parse(tokens)

        self.assertIsNone(next(iterator))

    def test_parses_two_optional_elements_returns_two_elements(self):
        tokens = Iterator(2 * [self.token] + [None])
        repetition = Repetition(self.mock, 0)

        tree, _ = repetition.parse(tokens)

        self.assertEqual(2 * [self.node], tree)

    def test_parses_two_optional_elements_returns_advanced_iterator(self):
        tokens = Iterator(2 * [self.token] + [None])
        repetition = Repetition(self.mock, 0)

        _, iterator = repetition.parse(tokens)

        self.assertIsNone(next(iterator))

    def test_parses_mismatched_required_element_returns_none(self):
        tokens = Iterator([None])
        repetition = Repetition(self.mock, 1)

        tree, _ = repetition.parse(tokens)

        self.assertIsNone(tree)

    def test_parses_partly_matched_repetition_returns_none(self):
        tokens = Iterator([self.token, None])
        repetition = Repetition(self.mock, 2)

        tree, _ = repetition.parse(tokens)

        self.assertIsNone(tree)

    def test_parses_longer_partly_matched_repetition_returns_none(self):
        tokens = Iterator([2 * [self.token], None])
        repetition = Repetition(self.mock, 2)

        tree, _ = repetition.parse(tokens)

        self.assertIsNone(tree)

    def test_parses_exactly_matched_repetition_returns_elements(self):
        tokens = Iterator(2 * [self.token] + [None])
        repetition = Repetition(self.mock, 2)

        tree, _ = repetition.parse(tokens)

        self.assertEqual(2 * [self.node], tree)

    def test_parses_longer_match_returns_elements(self):
        tokens = Iterator(5 * [self.token] + [None])
        repetition = Repetition(self.mock, 3)

        tree, _ = repetition.parse(tokens)

        self.assertEqual(5 * [self.node], tree)

    def test_parses_longer_match_returns_advanced_iterator(self):
        tokens = Iterator(2 * [self.token] + [None])
        repetition = Repetition(self.mock, 1)

        _, iterator = repetition.parse(tokens)

        self.assertIsNone(next(iterator))

    def test_max_value_less_than_one_raises(self):
        with self.assertRaises(ValueError):
            Repetition(self.mock, 0, 0)

    def test_max_value_less_than_min_value_raises(self):
        with self.assertRaises(ValueError):
            Repetition(self.mock, 2, 1)

    def test_parses_less_than_max_number_returns_elements(self):
        tokens = Iterator(2 * [self.token] + [None])
        repetition = Repetition(self.mock, 1, 3)

        tree, _ = repetition.parse(tokens)

        self.assertEqual(2 * [self.node], tree)

    def test_parses_exactly_max_number_returns_elements(self):
        tokens = Iterator(3 * [self.token] + [None])
        repetition = Repetition(self.mock, 3, 3)

        tree, _ = repetition.parse(tokens)

        self.assertEqual(3 * [self.node], tree)

    def test_parses_at_most_max_elements_even_if_more_were_given(self):
        tokens = Iterator(4 * [self.token] + [None])
        repetition = Repetition(self.mock, 2, 3)

        tree, _ = repetition.parse(tokens)

        self.assertEqual(3 * [self.node], tree)

    def test_returns_advanced_iterator_when_max_elements_were_read(self):
        tokens = Iterator(4 * [self.token] + [None])
        repetition = Repetition(self.mock, 2, 3)

        _, iterator = repetition.parse(tokens)

        self.assertEqual(self.node, next(iterator))
        self.assertIsNone(next(iterator))


class TestPlaceholder(unittest.TestCase):

    def test_placeholder_object_forwards_call(self):
        placeholder = Placeholder()
        object_ = Mock()
        object_.parse.return_value = 42

        placeholder.set_object(object_)

        self.assertEqual(42, placeholder.parse("tokens"))


class TestLexerChanger(unittest.TestCase):

    @staticmethod
    def test_lexer_changer_push_lexer():
        lxr = Mock()
        lc = LexerChanger("lexy")

        lc.parse(lxr)

        lxr.push.assert_called_once_with("lexy")
        lxr.pop.assert_not_called()

    @staticmethod
    def test_lexer_changer_pop_lexer():
        lxr = Mock()
        lc = LexerChanger("")

        lc.parse(lxr)

        lxr.pop.assert_called_once()
        lxr.push.assert_not_called()

    def test_parse_lexer_returns_empty_tree(self):
        lc = LexerChanger("")
        tree, _ = lc.parse(Mock())

        self.assertEqual([], tree)

    def test_parse_lexer_returns_unchanged_tokens(self):
        lc = LexerChanger("")
        lxr = Mock()
        _, tokens = lc.parse(lxr)

        self.assertEqual(lxr, tokens)
