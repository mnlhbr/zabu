import unittest
from unittest.mock import Mock, MagicMock
import itertools
import typing
from dataclasses import dataclass
from zabu import lexer
from zabu.lexer import Pattern, Token, Config
from zabu.util import Iterator
from zabu.lexer import LexerIterator
from zabu.lexer import create_lexer_iterator


def _lex(patterns, chars, config=Config()):
    lex = lexer.Lexer(patterns, config)
    return lex.process(Iterator(chars))[0]


def _lex2(patterns, iterator, config=Config()):
    lex = lexer.Lexer(patterns, config)
    return lex.process(iterator)


def _ptn(name, value=None):
    if value is None:
        value = name
    return Pattern(name, value)


def _tkn(name, value=None):
    if value is None:
        value = name
    return Token(name, value)


def _wtkn(string):
    return _tkn("wildcard", string)


class TestLexicalAnalyzer(unittest.TestCase):

    def test_return_empty_list_if_input_is_empty(self):
        self.assertEqual([], _lex([], ""))
        self.assertEqual([], _lex([_ptn("n", "v")], ""))

    def test_process_single_one_character_pattern(self):
        for name, value, count in itertools.product(["n", "o"],
                                                    ["v", "w"],
                                                    range(4)):
            with self.subTest(name=name, value=value, count=count):
                self.assertEqual(
                    [_tkn(name, value)] * count,
                    _lex([_ptn(name, value)], value * count))

    def test_process_multiple_one_character_pattern(self):
        char2name = {"v": "n", "w": "o"}
        patterns = [_ptn("n", "v"), _ptn("o", "w")]

        for chars in itertools.permutations("vvww"):
            tokens = [_tkn(char2name[char], char) for char in chars]

            with self.subTest(chars=chars):
                self.assertEqual(tokens, _lex(patterns, chars))

    def test_drop_one_of_two_tokens(self):
        patterns = [_ptn("n", "v"), _ptn("a", "b")]
        config = Config(wildcard_mode=True, drop_token_names=["n"])

        self.assertFalse(_tkn("n", "v") in _lex(patterns, "vbbcva", config))

    def test_drop_multiple_tokes(self):
        patterns = [_ptn("n", "v"), _ptn("a", "b"), _ptn("c", "d")]
        config = Config(wildcard_mode=True, drop_token_names=["n", "c"])

        self.assertFalse(_tkn("n", "v") in _lex(patterns, "bxvddvb", config))
        self.assertFalse(_tkn("c", "d") in _lex(patterns, "bxvddvb", config))


class TestLexerGetNextSimplePatterns(unittest.TestCase):

    def setUp(self):
        self.patterns = [_ptn("a"), _ptn("b"),
                         _ptn("c"), _ptn("x", "abc")]

    def test_get_first_and_only_token(self):
        lex = lexer.Lexer(self.patterns)
        lex.chars = Iterator("a")

        self.assertEqual(_tkn("a"), next(lex))

    def test_get_two_tokens(self):
        lex = lexer.Lexer(self.patterns)
        lex.chars = Iterator("ab")

        self.assertEqual([_tkn("a"), _tkn("b")], [next(lex), next(lex)])

    def test_get_more_tokens(self):
        lex = lexer.Lexer(self.patterns)
        lex.chars = Iterator("acabc")
        tokens = [_tkn("a"), _tkn("c"), _tkn("x", "abc")]

        self.assertEqual(tokens, [next(lex) for _ in range(3)])

    def test_raises_when_there_are_no_more_tokens_left(self):
        lex = lexer.Lexer(self.patterns)
        lex.chars = Iterator("a")
        next(lex)

        with self.assertRaises(StopIteration):
            next(lex)

    def test_fill_list_with_lexer(self):
        lex = lexer.Lexer(self.patterns)
        lex.chars = Iterator("abccba")
        tokens = [_tkn("x", "abc"), _tkn("c"), _tkn("b"), _tkn("a")]

        self.assertEqual(tokens, list(lex))


class TestLexerGetNextWildcardPattern(unittest.TestCase):

    def setUp(self):
        patterns = [_ptn("ac"), _ptn("ab")]

        config = Config(wildcard_mode=True)

        self.lex = lexer.Lexer(patterns, config)

    def test_get_single_char_wildcard(self):
        self.lex.chars = Iterator("x")

        self.assertEqual(_wtkn("x"), next(self.lex))

    def test_get_longer_wildcard(self):
        self.lex.chars = Iterator("xxx")

        self.assertEqual(_wtkn("xxx"), next(self.lex))

    def test_get_patterns(self):
        self.lex.chars = Iterator("acabac")
        tokens = [_tkn("ac"), _tkn("ab"), _tkn("ac")]

        self.assertEqual(tokens, list(self.lex))

    def test_get_wildcard_with_pattern_prefix(self):
        self.lex.chars = Iterator("aa")

        self.assertEqual(_wtkn("aa"), next(self.lex))

    def test_get_wildcard_with_multiple_pattern_prefix(self):
        self.lex.chars = Iterator("aaaa")

        self.assertEqual(_wtkn("aaaa"), next(self.lex))

    def test_get_token_after_multiple_pattern_prefix(self):
        self.lex.chars = Iterator("aaaac")

        self.assertEqual(_wtkn("aaa"), next(self.lex))
        self.assertEqual(_tkn("ac"), next(self.lex))


class TestMultipleLexerInvocations(unittest.TestCase):

    def test_process_single_token(self):
        patterns = [_ptn("n", "v")]
        tokens = [_tkn("n", "v")]

        lexer_ = lexer.Lexer(patterns)

        self.assertEqual(tokens, lexer_.process(Iterator("v"))[0])
        self.assertEqual(tokens, lexer_.process(Iterator("v"))[0])
        self.assertEqual(tokens, lexer_.process(Iterator("v"))[0])

    def test_process_multiple_patterns_and_tokens(self):
        patterns = [_ptn("a", "b"), _ptn("c", "d")]
        tokens_aca = [_tkn("a", "b"), _tkn("c", "d"), _tkn("a", "b")]
        tokens_cca = [_tkn("c", "d"), _tkn("c", "d"), _tkn("a", "b")]

        lexer_ = lexer.Lexer(patterns)

        self.assertEqual(tokens_aca, lexer_.process(Iterator("bdb"))[0])
        self.assertEqual(tokens_cca, lexer_.process(Iterator("ddb"))[0])
        self.assertEqual(tokens_aca, lexer_.process(Iterator("bdb"))[0])


class TestLexerMultiCharacterPattern(unittest.TestCase):

    def test_process_one_pattern(self):
        patterns = [_ptn("m", "vv")]
        chars = "vv"
        self.assertEqual([_tkn("m", "vv")], _lex(patterns, chars))

    def test_process_one_pattern_multiple_times(self):
        patterns = [_ptn("m", "vv")]
        chars = "vv" * 3
        self.assertEqual(3 * [_tkn("m", "vv")], _lex(patterns, chars))

    def test_process_multiple_patterns(self):
        patterns = [_ptn("a", "bb"), _ptn("c", "de")]
        chars = "debbdedebb"
        tokens = [_tkn("c", "de"), _tkn("a", "bb"), _tkn("c", "de"),
                  _tkn("c", "de"), _tkn("a", "bb")]
        self.assertEqual(tokens, _lex(patterns, chars))

    def test_process_multiple_patterns_with_same_prefix(self):
        patterns = [_ptn("a", "x"), _ptn("b", "xy"), _ptn("c", "xyz")]
        chars = "xxyzxy"
        tokens = [_tkn("a", "x"), _tkn("c", "xyz"), _tkn("b", "xy")]
        self.assertEqual(tokens, _lex(patterns, chars))

    def test_process_patterns_with_matching_prefix_and_center(self):
        patterns = [_ptn("a", "x"), _ptn("b", "xyz"), _ptn("c", "ya")]
        chars = "xya"
        tokens = [_tkn("a", "x"), _tkn("c", "ya")]
        self.assertEqual(tokens, _lex(patterns, chars))

    def test_process_failing_pattern_with_multiple_matching_subpatterns(self):
        patterns = [_ptn("a"), _ptn("bc"),
                    _ptn("abc"), _ptn("abcde"),
                    _ptn("dx")]
        chars = "abcdx"
        tokens = [_tkn("abc"), _tkn("dx")]
        self.assertEqual(tokens, _lex(patterns, chars))

    def test_process_pattern_with_multiple_matching_subpatterns(self):
        patterns = [_ptn("a"), _ptn("bc"),
                    _ptn("abc"), _ptn("abcde"),
                    _ptn("dx")]
        chars = "abcde"
        tokens = [_tkn("abcde")]
        self.assertEqual(tokens, _lex(patterns, chars))


class TestWildcardMode(unittest.TestCase):

    def setUp(self):
        self.p0 = [_ptn("p0", "x")]
        self.config = Config(wildcard_mode=True)

    def test_process_one_pattern_at_end_of_string(self):
        tokens = [_wtkn("abc"), _tkn("p0", "x")]
        chars = "abcx"
        self.assertEqual(tokens, _lex(self.p0, chars, self.config))

    def test_process_one_pattern_at_start_of_string(self):
        tokens = [_tkn("p0", "x"), _wtkn("abc")]
        chars = "xabc"
        self.assertEqual(tokens, _lex(self.p0, chars, self.config))

    def test_process_one_pattern_inside_string(self):
        tokens = [_wtkn("a"),
                  _tkn("p0", "x"),
                  _wtkn("bc")]
        chars = "axbc"
        self.assertEqual(tokens, _lex(self.p0, chars, self.config))

    def test_process_only_wildcard_characters(self):
        for chars in ["abc", "def", "ghi"]:
            with self.subTest(chars=chars):
                self.assertEqual([_wtkn(chars)],
                                 _lex([], chars, self.config))

    def test_process_multi_character_patterns(self):
        pattern = [_ptn("a", "bb"), _ptn("c", "de")]
        tokens = [_wtkn("xyz"), _tkn("a", "bb"),
                  _wtkn("xxx"), _tkn("c", "de"),
                  _wtkn("z")]

        chars = "xyzbbxxxdez"

        self.assertEqual(tokens, _lex(pattern, chars, self.config))

    def test_process_multi_char_patterns_with_wildcard_suffix(self):
        pattern = [_ptn("a", "aa")]
        tokens = [_wtkn("xx"), _tkn("a", "aa"),
                  _wtkn("abc")]

        chars = "xxaaabc"

        self.assertEqual(tokens, _lex(pattern, chars, self.config))

    def test_process_pattern_after_failed_multi_char_pattern(self):
        pattern = [_ptn("a", "aa"), _ptn("b")]
        tokens = [_wtkn("xa"), _tkn("b")]

        chars = "xab"

        self.assertEqual(tokens, _lex(pattern, chars, self.config))

    def test_process_multi_char_pattern_which_trigger_double_wildcard(self):
        pattern = [_ptn("a", "aa")]
        tokens = [_wtkn("xax"),
                  _tkn("a", "aa"),
                  _wtkn("xax")]

        chars = "xaxaaxax"

        self.assertEqual(tokens, _lex(pattern, chars, self.config))

    def test_process_pattern_which_is_prefix_of_other_pattern(self):
        patterns = [_ptn("a", "a"), _ptn("abc", "abc")]
        tokens = [_tkn("a"), _wtkn("bx")]

        self.assertEqual(tokens, _lex(patterns, "abx", self.config))

    def test_process_wildcard_which_is_part_of_lexeme(self):
        patterns = [_ptn("3", "aaa"), _ptn("6", "aaaaaa")]
        tokens = [_wtkn("aa")]

        self.assertEqual(tokens, _lex(patterns, "aa", self.config))

    def test_process_wildcard_which_is_part_of_lexeme_after_subtoken(self):
        patterns = [_ptn("3", "aaa"), _ptn("6", "aaaaaa")]
        tokens = [_tkn("3", "aaa"), _wtkn("aa")]

        self.assertEqual(tokens, _lex(patterns, "aaaaa", self.config))

    def test_dont_join_wildcards_when_disabled(self):
        config = Config(wildcard_mode=True,
                        join_wildcards=False,
                        drop_token_names=["p0"])
        tokens = [_wtkn("a"), _wtkn("b")]

        self.assertEqual(tokens, _lex(self.p0, "axb", config))


class TestEscapeCharacter(unittest.TestCase):

    def test_process_simple_escaped_character(self):
        pattern = [_ptn("a")]
        config = lexer.Config(wildcard_mode=True, escape_character="\\")
        lexer_ = lexer.Lexer(pattern, config)

        tokens = [_wtkn("a")]

        self.assertEqual(tokens, lexer_.process(Iterator("\\a"))[0])


class _Node(typing.NamedTuple):
    name: str
    content: typing.Union[list, str]


@dataclass
class _Lexer:

    def __init__(self, patterns, config=Config()):
        self.patterns = patterns
        self.config = config


@dataclass
class _LexerIterator:

    def __init__(self, lexers, start, chars):
        self.lexers = lexers
        self.start = start
        self.chars = chars


@dataclass
class _Config:

    name: str = ""
    abc: str = ""
    xyz: str = ""
    drop_token_names: typing.List[str] = ()


def _create_sublexer_tree(kvpairs):
    tree = []

    for key, values in kvpairs.items():
        element = []
        element.append(_Node("name", [_Node("lexeme", key)]))
        element.append(_Node("lexeme", "="))
        for value in values:
            if value.startswith("'"):
                value = value[1:-1]
                element.append(_Node("sqvalue", [
                    _Node("lexeme", "'"),
                    _Node("lexeme", value),
                    _Node("lexeme", "'")
                ]))
            else:
                element.append(_Node("value", [_Node("lexeme", value)]))

        element.append(_Node("eol", [_Node("lexeme", "\n")]))

        tree.append(_Node("keyvalue", element))

    return tree


def _create_key_value_tree(kvpairs):
    return _create_sublexer_tree(kvpairs)


class TestCreateLexerIterator(unittest.TestCase):

    @staticmethod
    def cli(config, trees, pattern_strings):
        return create_lexer_iterator(config, trees, pattern_strings,
                                     _Config, _Lexer, _LexerIterator)

    def test_simple_main_lexer_with_one_pattern(self):
        tree = _create_key_value_tree(
            {"name": ["flexy"],
             "xyz": ["123"]})
        lexer_ = self.cli(tree, {}, {None: ["1"]})
        lexers = lexer_.lexers

        self.assertIsNone(lexer_.start)
        self.assertIn(None, lexers)
        self.assertEqual("flexy", lexers[None].config.name)
        self.assertEqual("123", lexers[None].config.xyz)

    def test_simple_additional_lexer_with_one_pattern(self):
        tree = _create_key_value_tree(
            {"name": ["subby"],
             "abc": ["42"]})
        lexer_ = self.cli({}, [tree], {"subby": ["2"], None: []})

        self.assertIn("subby", lexer_.lexers)
        self.assertEqual("subby", lexer_.lexers["subby"].config.name)
        self.assertEqual("42", lexer_.lexers["subby"].config.abc)


class TestLexerIterator(unittest.TestCase):

    def setUp(self):
        lexer_one = Mock(return_value=1)

        lexer_two = Mock(return_value=2)

        self.lexers = {"one": lexer_one,
                       "two": lexer_two}

        chars = Mock()
        chars.copy.return_value = 42
        self.chars = chars

    def test_first_lexer_returns_tokens(self):
        li = LexerIterator(self.lexers, "one", self.chars)
        self.assertEqual(next(li), 1)
        self.assertEqual(next(li), 1)

    def test_second_lexer_returns_tokens(self):
        li = LexerIterator(self.lexers, "two", self.chars)
        self.assertEqual(next(li), 2)
        self.assertEqual(next(li), 2)

    def test_second_lexer_returns_tokens_after_push(self):
        li = LexerIterator(self.lexers, "one", self.chars)
        li.push("two")
        self.assertEqual(next(li), 2)
        self.assertEqual(next(li), 2)

    def test_first_lexer_returns_token_after_push_and_pop(self):
        li = LexerIterator(self.lexers, "one", self.chars)
        li.push("two")
        li.pop()
        self.assertEqual(next(li), 1)
        self.assertEqual(next(li), 1)

    def test_pop_on_empty_lexer_stack_pop_raises_index_error(self):
        li = LexerIterator(self.lexers, "one", self.chars)

        with self.assertRaises(IndexError):
            li.pop()

    def test_lexer_iterator_copy_works(self):
        chars = MagicMock()
        chars.copy.return_value = chars

        li = LexerIterator(self.lexers, "one", chars)
        li.push("two")
        li = li.copy()
        self.assertEqual(next(li), 2)

    def test_lexer_iterator_returns_self_as_iterator(self):
        li = LexerIterator(self.lexers, "one", self.chars)

        self.assertIs(li, iter(li))
