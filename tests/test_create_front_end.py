import unittest
import typing
from zabu.grammar import create_front_end
from zabu import util


class _Node(typing.NamedTuple):
    name: str
    content: typing.Union[list, str]


def _lex(lexeme):
    return _Node("lexeme", lexeme)


def _nlex(name, lexeme):
    return _Node(name, [_lex(lexeme)])


def _nqlex(name, lexeme):
    return _Node(name, [_lex("'"),
                        _lex(lexeme),
                        _lex("'")])


def _ntrm(lexeme):
    return _nlex("nonterminal", lexeme)


def _trm(lexeme):
    return _nqlex("terminal", lexeme)


def _node(name):
    return _Node(name, "")


def _chlxr(name=None):
    if name is None:
        name = [_lex(" ")]
    else:
        name = [_lex(name), _lex(" ")]

    return _Node("change_lexer", [_lex("/"),
                                  _Node("lexer_name", name)])


def _kv_tree(kvpairs):
    tree = []

    for key, values in kvpairs.items():
        element = []
        element.append(_Node("name", [_lex(key)]))
        element.append(_lex("="))

        for value in values:
            if value.startswith("'"):
                value = value[1:-1]
                element.append(_nqlex("sqvalue", value))
            else:
                element.append(_nlex("value", value))

        element.append(_Node("eol", [_lex("\n")]))

        tree.append(_Node("keyvalue", element))

    return tree


def _rule(name, rule):
    invisible = []

    if name.startswith("."):
        name = name[1:]
        invisible = [_nlex("invisible", ".")]

    return _Node("rule", [
        _Node("lhs", invisible + [_nlex("name", name)]),
        _lex(":="),
        _Node("rhs", rule),
        _nlex("eol", "\n")
    ])


def _rs(string):
    rule = []

    rmap = {"&": "and",
            "|": "or",
            "?": "repetition01",
            "*": "repetition0inf",
            "+": "repetition1inf",
            "(": "leftparenthesis",
            ")": "rightparenthesis"}

    for s in string.split(" "):
        if s in rmap:
            rule.append(_nlex(rmap[s], s))
        elif s.startswith("'") and s.endswith("'"):
            rule.append(_trm(s[1:-1]))
        elif s == "/":
            rule.append(_chlxr())
        elif s.startswith("/"):
            rule.append(_chlxr(s[1:]))
        else:
            rule.append(_ntrm(s))

    return rule


def _abc_tree():

    rules = [
        _rule(".d", _rs("p e +")),
        _rule(".p", _rs("'a' p 'b' | 'a' 'b'")),
        _rule(".e", _rs("'c'"))
    ]

    return [_Node("rules", rules)]


class TestCreateFrontEndWithABC(unittest.TestCase):

    def setUp(self):
        self.tree = _abc_tree()

    def act(self, string):
        lexer, parser = create_front_end(self.tree)

        tokens, _ = lexer.process(util.Iterator(string))
        tree, _ = parser.parse(util.Iterator(tokens))

        expected = [_lex(s) for s in string]

        return expected, tree

    def test_abc(self):
        self.assertEqual(*self.act("abc"))
        self.assertEqual(*self.act("aabbc"))
        self.assertEqual(*self.act("aabbcc"))
        self.assertEqual(*self.act("aabbcccc"))
        self.assertEqual(*self.act("aaabbbc"))
        self.assertEqual(*self.act("aaabbbcccccc"))


def _simple_math_tree():
    config = _kv_tree({"drop_token_names": [' ']})

    rules = [
        _rule(".l", _rs("( c | t ) +")),
        _rule("c", _rs("'#' /comment 'wildcard' f /")),
        _rule("t", _rs("s e")),
        _rule("s", _rs("p '+' p")),
        _rule(".s", _rs("p")),
        _rule("p", _rs("d '*' d")),
        _rule(".p", _rs("d")),
        _rule("d", _rs("'1' | '2' | '3'")),
        _rule("e", _rs("'\n'")),
        # TODO: remove 'f' and use 'e' after lexer patterns fix
        _rule("f", _rs("'\n'"))
    ]

    sublexer = _kv_tree({"name": ["comment"],
                         "wildcard_mode": ["true"],
                         "escape_character": ["\\"]})

    return [_Node("config", config),
            _Node("rules", rules),
            _Node("lexer", sublexer)]


class TestCreateFrontEndWithSimpleMath(unittest.TestCase):

    def setUp(self):
        self.tree = _simple_math_tree()

    def act(self, string):
        lexer, parser = create_front_end(self.tree)

        lexer.set_chars(util.Iterator(string))
        tree, _ = parser.parse(lexer)

        return tree

    def test_single_comment(self):
        expected = [_Node("c", [_lex("#"), _lex(" a b c"), _nlex("f", "\n")])]
        self.assertEqual(expected, self.act("# a b c\n"))

    def test_comments_with_digits(self):
        expected = [_Node("c", [_lex("#"), _lex(" 1 2 3"), _nlex("f", "\n")]),
                    _Node("c", [_lex("#"), _lex("# 42 #"), _nlex("f", "\n")])]
        self.assertEqual(expected, self.act("# 1 2 3\n## 42 #\n"))

    def test_multi_line_comment(self):
        expected = [_Node("c", [_lex("#"), _lex(" abc\ndef"),
                                _nlex("f", "\n")])]
        self.assertEqual(expected, self.act("# abc\\\ndef\n"))

    def test_single_digit(self):
        expected = [_Node("t", [_nlex("d", "1"), _nlex("e", "\n")])]
        self.assertEqual(expected, self.act("1\n"))

    def test_single_digit_with_spaces(self):
        expected = [_Node("t", [_nlex("d", "1"), _nlex("e", "\n")])]
        self.assertEqual(expected, self.act("  1  \n"))

    def test_sum(self):
        expected = [_Node("t", [_Node("s", [_nlex("d", "1"),
                                            _lex("+"),
                                            _nlex("d", "2")]),
                                _nlex("e", "\n")])]
        self.assertEqual(expected, self.act("1+2\n"))
        self.assertEqual(expected, self.act("1 + 2\n"))

    def test_product(self):
        expected = [_Node("t", [_Node("p", [_nlex("d", "2"),
                                            _lex("*"),
                                            _nlex("d", "3")]),
                                _nlex("e", "\n")])]
        self.assertEqual(expected, self.act("2*3\n"))
        self.assertEqual(expected, self.act("2 * 3\n"))

    def test_sum_product(self):
        expected = [_Node("t", [_Node("s", [_nlex("d", "1"),
                                            _lex("+"),
                                            _Node("p", [_nlex("d", "2"),
                                                        _lex("*"),
                                                        _nlex("d", "3")])]),
                                _nlex("e", "\n")])]
        self.assertEqual(expected, self.act("1+2*3\n"))
        self.assertEqual(expected, self.act("1 + 2 * 3\n"))

    def test_comments_and_sum(self):
        expected = [_Node("c", [_lex("#"),
                                _lex(" *comment*"),
                                _nlex("f", "\n")]),
                    _Node("t", [_Node("s", [_nlex("d", "2"),
                                            _lex("+"),
                                            _nlex("d", "3")]),
                                _nlex("e", "\n")]),
                    _Node("c", [_lex("#"),
                                _lex("end"),
                                _nlex("f", "\n")])]
        self.assertEqual(expected, self.act("# *comment*\n2 +3\n#end\n"))
