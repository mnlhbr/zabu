import sys
from importlib.resources import open_text
from zabu.grammars import grammar as saved_grammar
from zabu import util
from zabu import grammar
import zabu.grammars
import zabu.intermediate.from_zabu
import zabu.intermediate.to_html


def _print_tree(tree, width=4, indentation=""):
    for node in tree:
        if node.name == "lexeme":
            print(f'{indentation}"{node.content}"')
        else:
            print(f"{indentation}{node.name}")
            if not node.name.startswith("eol"):
                _print_tree(node.content, width, f"{indentation}{width * ' '}")


def _print_grammar_py_tree(tree):
    print("from zabu.parser import Node\n")
    print("tree = [")
    end = ""

    for node in tree:
        print(end, end="")

        if node.name in ("config", "rules", "lexer"):
            print(f"  Node(name='{node.name}', content=[")
            print("    ", end="")
            print(",\n    ".join(map(str, node.content)), end="")
            print("])", end="")
        else:
            print(f"  {node}", end="")

        end = ",\n"

    print("]")


def main():
    lexer_, parser_ = grammar.create_front_end(saved_grammar.tree)

    with open_text(zabu.grammars, "grammar.txt") as file_:
        filecontent = file_.read()
    lexer_.set_chars(util.Iterator(filecontent))
    tree, _ = parser_.parse(lexer_)
    lexer_, parser_ = grammar.create_front_end(tree)

    with open_text(zabu.grammars, "grammar.txt") as file_:
        filecontent = file_.read()
    lexer_.set_chars(util.Iterator(filecontent))
    tree, _ = parser_.parse(lexer_)
    lexer_, parser_ = grammar.create_front_end(tree)

    # print(tree)
    # _print_tree(tree)
    # _print_grammar_py_tree(tree)

    with open_text(zabu.grammars, "syntax.txt") as file_:
        filecontent = file_.read()

    lexer_.set_chars(util.Iterator(filecontent))
    tree, _ = parser_.parse(lexer_)
    wiki_lexer, wiki_parser = grammar.create_front_end(tree)

    zabu_file = "demo.zabu"
    if len(sys.argv) == 2:
        zabu_file = sys.argv[1]

    with open(zabu_file, "r", encoding="UTF-8") as file_:
        filecontent = file_.read()

    wiki_lexer.set_chars(util.Iterator(filecontent))
    tree, _ = wiki_parser.parse(wiki_lexer)

    # print("\n".join((str(t) for t in tokens)))
    # print(tree)
    if tree is not None:
        _print_tree(tree)
    # print(zabu.intermediate.to_html.convert(
    #     zabu.intermediate.from_zabu.convert(tree)), end="")


if __name__ == "__main__":
    main()
