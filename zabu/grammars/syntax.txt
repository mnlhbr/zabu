wildcard_mode = true
escape_character = '\\'


.document                    := part*
.part                        := heading | paragraph | eop | ul0

.heading                     := heading1 | heading2 | heading3 | heading4 | heading5 | heading6
heading1                     := '=' space heading_content eol
heading2                     := '==' space heading_content eol
heading3                     := '===' space heading_content eol
heading4                     := '====' space heading_content eol
heading5                     := '=====' space heading_content eol
heading6                     := '======' space heading_content eol
heading_content              := plain_text

paragraph                    := wiki_line paragraph_part*
.plain_line                  := plain_text eol
.wiki_line                   := wiki_text eol
.plain_text                  := word (space | word)*
.wiki_text                   := (word | space | internal_link | external_link)+
.paragraph_part              := wiki_line | ul0

internal_link                := '[' internal_link_url link_description? ']'
external_link                := '[[' external_link_url link_description? ']]'
internal_link_url            := 'wildcard'
external_link_url            := 'wildcard'
link_description             := '|' link_text
link_text                    := word

space                        := ' '
eop                          := eol+
word                         := 'wildcard'
eol                          := '
'

ul0 unordered_list           := ul0i+
ul0i unordered_list_item     := '  * ' ul0ic
.ul0ic                       := wiki_line ul0icc*
.ul0icc                      := '    ' wiki_line
.ul0icc                      := ul1
ul1 unordered_list           := ul1i+
ul1i unordered_list_item     := '    * ' ul1ic
.ul1ic                       := wiki_line ul1icc*
.ul1icc                      := '      ' wiki_line
.ul1icc                      := ul2
ul2 unordered_list           := ul2i+
ul2i unordered_list_item     := '      * ' ul2ic
.ul2ic                       := wiki_line ul2icc*
.ul2icc                      := '        ' wiki_line
