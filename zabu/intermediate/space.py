from dataclasses import dataclass
from zabu.intermediate import Type


@dataclass
class Space:

    string: str

    @staticmethod
    def type():
        return Type.SPACE


@dataclass
class EOL:

    string: str

    @staticmethod
    def type():
        return Type.EOL
