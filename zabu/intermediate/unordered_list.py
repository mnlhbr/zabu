from dataclasses import dataclass
from zabu.intermediate import Type


@dataclass
class UnorderedList:

    @dataclass
    class ListItem:
        content: list

    items: list[ListItem]

    def __init__(self, items=None):
        self.items = items if items is not None else []

    @staticmethod
    def type():
        return Type.UNORDERED_LIST
