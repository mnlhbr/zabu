import enum


@enum.unique
class Type(enum.Enum):
    HEADING = enum.auto()
    INTERNAL_LINK = enum.auto()
    EXTERNAL_LINK = enum.auto()
    PARAGRAPH = enum.auto()
    WORD = enum.auto()
    UNORDERED_LIST = enum.auto()
    SPACE = enum.auto()
    EOL = enum.auto()
