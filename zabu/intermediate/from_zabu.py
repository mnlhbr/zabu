from zabu.intermediate.heading import Heading
from zabu.intermediate.paragraph import Paragraph
from zabu.intermediate.word import Word
from zabu.intermediate.space import Space, EOL
from zabu.intermediate.internal_link import InternalLink
from zabu.intermediate.external_link import ExternalLink
from zabu.intermediate.unordered_list import UnorderedList


def _get_lexeme(node):
    return node.content[0].content


def _find_node(tree, name):
    for node in tree.content:
        if node.name == name:
            return node

    return None


def internal_link(tree):
    url = _get_lexeme(_find_node(tree, "internal_link_url"))
    description = _find_node(tree, "link_description")

    if description is not None:
        description = text(_find_node(description, "link_text").content)

    return InternalLink(url, description)


def external_link(tree):
    url = _get_lexeme(_find_node(tree, "external_link_url"))
    description = _find_node(tree, "link_description")

    if description is not None:
        description = text(_find_node(description, "link_text").content)

    return ExternalLink(url, description)


def text(tree):
    content = []

    for element in tree:
        if element.name == "word":
            content.append(Word(_get_lexeme(element)))
        elif element.name == "space":
            content.append(Space(_get_lexeme(element)))
        elif element.name == "eol":
            content.append(EOL(_get_lexeme(element)))
        elif element.name == "internal_link":
            content.append(internal_link(element))
        elif element.name == "external_link":
            content.append(external_link(element))
        elif element.name == "unordered_list":
            content.append(unordered_list(element))

    return content


def heading(tree):
    map_level = {f"heading{i}": l for i, l in enumerate(Heading.Level)}

    level = map_level[tree.name]

    return Heading(level, text(_find_node(tree, "heading_content").content))


def unordered_list_item(tree):
    return UnorderedList.ListItem(text(tree.content))


def unordered_list(tree):
    items = []

    for element in tree.content:
        items.append(unordered_list_item(element))

    return UnorderedList(items)


def paragraph(tree):
    return Paragraph(text(tree.content))


def convert(tree):
    content = []

    for element in tree:
        if element.name == "paragraph":
            content.append(paragraph(element))
        elif element.name.startswith("heading"):
            content.append(heading(element))
        elif element.name == "unordered_list":
            content.append(unordered_list(element))

    return content
