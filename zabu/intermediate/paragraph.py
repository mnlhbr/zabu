from dataclasses import dataclass
from zabu.intermediate import Type


@dataclass
class Paragraph:

    content: list

    @staticmethod
    def type():
        return Type.PARAGRAPH
