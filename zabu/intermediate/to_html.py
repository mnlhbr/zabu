from zabu.intermediate import Type
from zabu.intermediate.heading import Heading
from zabu.intermediate.indented_document import IndentedDocument


def _link(html, link, link_type):
    url = link.url

    if link_type == Type.INTERNAL_LINK:
        url += ".html"

    html.append(f'<a href="{url}">')
    _text(html, link.description)
    html.append("</a>")

    return html


def _text(html, text):
    for element in text:
        if element.type() == Type.WORD:
            html.append(element.string)
        elif element.type() == Type.SPACE:
            html.append(" ")
        elif element.type() == Type.EOL:
            html.new_line()
        elif element.type() == Type.INTERNAL_LINK:
            _link(html, element, element.type())
        elif element.type() == Type.EXTERNAL_LINK:
            _link(html, element, element.type())
        elif element.type() == Type.UNORDERED_LIST:
            _unordered_list(html, element)

    return html


def _heading(html, heading):
    heading_type = {Heading.Level.L1: "h1",
                    Heading.Level.L2: "h2",
                    Heading.Level.L3: "h3",
                    Heading.Level.L4: "h4",
                    Heading.Level.L5: "h5",
                    Heading.Level.L6: "h6"}[heading.level]

    html.append(f"<{heading_type}>")
    _text(html, heading.content)
    html.append_line(f"</{heading_type}>")

    return html


def _paragraph(html, paragraph):
    html.append_line("<p>").right()
    _text(html, paragraph.content)
    html.left().append_line("</p>")

    return html


def _unordered_list(html, ulist):
    html.append_line("<ul>").right()

    for item in ulist.items:
        html.append_line("<li>").right()
        _text(html, item.content)
        html.left().append_line("</li>")

    html.left().append_line("</ul>")

    return html


def convert(intermediate):
    html = IndentedDocument("    ")

    html.append_line("<!DOCTYPE html>")
    html.append_line("<html>").right()
    html.append_line("<head>").right()
    html.append_line("<meta charset=\"UTF-8\">")
    html.append_line("<title>ZaBu</title>")
    html.left().append_line("</head>")
    html.append_line("<body>").right()
    html.new_line()

    for element in intermediate:
        if element.type() == Type.HEADING:
            _heading(html, element)
        elif element.type() == Type.PARAGRAPH:
            _paragraph(html, element)
        elif element.type() == Type.UNORDERED_LIST:
            _unordered_list(html, element)
        html.new_line()

    html.left().append_line("</body>")
    html.left().append_line("</html>")

    return str(html)
