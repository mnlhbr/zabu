from dataclasses import dataclass
import enum
from zabu.intermediate import Type


@dataclass
class Heading:

    @enum.unique
    class Level(enum.Enum):
        L0 = enum.auto()
        L1 = enum.auto()
        L2 = enum.auto()
        L3 = enum.auto()
        L4 = enum.auto()
        L5 = enum.auto()
        L6 = enum.auto()

    level: Level
    content: list

    @staticmethod
    def type():
        return Type.HEADING
