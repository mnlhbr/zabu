class NegativeIndentationError(Exception):
    pass


class IndentedDocument:

    def __init__(self, indentation_string="  "):
        self._lines = []
        self._current_line = []
        self._indentation_level = 0
        self._indentation_string = indentation_string
        self._indented = False

    def append(self, string):
        if not self._indented:
            self._current_line.append(self._indentation_level *
                                      self._indentation_string)
            self._indented = True

        self._current_line.append(string)

        return self

    def append_line(self, string):
        self.append(string)
        self.new_line()

        return self

    def new_line(self):
        self._lines.append(self._current_line)
        self._current_line = [""]
        self._indented = False

        return self

    def right(self):
        self._indentation_level += 1

        return self

    def left(self):
        if self._indentation_level <= 0:
            raise NegativeIndentationError

        self._indentation_level -= 1

        return self

    def __str__(self):
        lines = self._lines

        lines.append(self._current_line)

        return "\n".join(map("".join, lines))
