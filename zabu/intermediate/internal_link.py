from dataclasses import dataclass
from zabu.intermediate import Type
from zabu.intermediate.word import Word


@dataclass
class InternalLink:

    url: str
    description: list

    def __init__(self, url, description=None):
        self.url = url
        self.description = (description if description is not None
                            else [Word(url)])

    @staticmethod
    def type():
        return Type.INTERNAL_LINK
