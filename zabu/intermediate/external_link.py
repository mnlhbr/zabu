from dataclasses import dataclass
from zabu.intermediate import Type
from zabu.intermediate.word import Word


@dataclass
class ExternalLink:

    url: str
    description: list

    def __init__(self, url, description=""):
        self.url = url
        self.description = (description if description is not None
                            else [Word(url)])

    @staticmethod
    def type():
        return Type.EXTERNAL_LINK
