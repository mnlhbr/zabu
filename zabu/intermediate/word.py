from dataclasses import dataclass
from zabu.intermediate import Type


@dataclass
class Word:

    string: str

    @staticmethod
    def type():
        return Type.WORD
