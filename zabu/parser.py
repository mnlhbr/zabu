import typing


class Node(typing.NamedTuple):
    name: str
    content: typing.Union[list, str]


class Terminal:

    def __init__(self, name):
        self._name = name

    def parse(self, tokens):
        token = next(tokens, None)

        if token is None:
            return None, None

        if token.name == self._name:
            return [Node("lexeme", token.value)], tokens

        return None, None


class Production:

    def __init__(self, production, name="", node_name=""):
        if not production:
            raise ValueError("production cannot be empty")

        self._production = production
        self._name = name

        self._node_name = node_name if node_name else name

    def parse(self, tokens):
        tree = []

        for element in self._production:
            part, tokens = element.parse(tokens)

            if part is None:
                return None, None

            tree += part

        if self._name:
            tree = [Node(self._node_name, tree)]

        return tree, tokens


class Option:

    def __init__(self, options):
        if not options:
            raise ValueError("option cannot be empty")

        self._options = options

    def parse(self, tokens):
        for option in self._options:
            tree, remaining_tokens = option.parse(tokens.copy())

            if tree is not None:
                return tree, remaining_tokens

        return None, None


class Repetition:

    # just some random magic numbers multiplied
    _default_max = round(42 * 6.28 * 2**3)

    def __init__(self, element, min_, max_=_default_max):
        if min_ < 0:
            raise ValueError("min number cannot be less than zero")
        if max_ < 1:
            raise ValueError("max number cannot be less than one")
        if max_ < min_:
            raise ValueError("max number cannot be less than min number")

        self._element = element
        self._min = min_
        self._max = max_

    def parse(self, tokens):
        tree = []
        count = 0

        while True:
            part, remaining_tokens = self._element.parse(tokens.copy())

            if part is None:
                break

            tree += part
            tokens = remaining_tokens
            count += 1

            if count == self._max:
                break

        if count < self._min:
            return None, None

        return tree, tokens


class Placeholder:

    def __init__(self):
        self._actual_object = None

    def set_object(self, object_):
        self._actual_object = object_

    def parse(self, tokens):
        return self._actual_object.parse(tokens)


class LexerChanger:

    def __init__(self, name):
        self._name = name

    def parse(self, tokens):
        if self._name:
            tokens.push(self._name)
        else:
            tokens.pop()

        return [], tokens
