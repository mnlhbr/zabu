import typing
import copy
import zabu.util

# TODO: remove unittest, only used for transition from sublexer to lexer
# iterator.
import unittest.mock


class Pattern(typing.NamedTuple):
    name: str
    value: str


class Token(typing.NamedTuple):
    name: str
    value: str


class _Transition(typing.NamedTuple):
    read: bool
    emit: str
    next_state: int


class Config(typing.NamedTuple):
    name: str = ""
    wildcard_mode: bool = False
    drop_token_names: typing.List[str] = []
    escape_character: str = ""
    join_wildcards: bool = True


class LexerIterator:

    def __init__(self, lexers, start, chars):
        self._lexers = lexers
        self._lexer = [self._lexers[start]]

        self._chars = chars.copy()

    def __iter__(self):
        return self

    def __next__(self):
        ret = self._lexer[-1](self._chars)
        self._chars = self._lexer[-1].chars
        return ret

    def push(self, name):
        self._lexers[name].chars = self._lexer[-1].chars
        self._lexer.append(self._lexers[name])

    def pop(self):
        if len(self._lexer) > 1:
            chars = self._lexer[-1].chars
            self._lexer.pop()
            self._lexer[-1].chars = chars
        else:
            raise IndexError("pop from empty lexer iterator stack")

    def set_chars(self, chars):
        # TODO: Remove or revise after lexer rework.
        self._chars = chars.copy()
        self._lexer[-1].chars = chars.copy()

    def process(self, chars):
        # TODO: Remove after lexer rework.
        return self._lexer[-1].process(chars)

    # TODO: use dunder copy
    def copy(self):
        # TODO: get rid of state in lexer after removal of sublexers
        li = copy.deepcopy(self)
        li._lexer[-1].chars = self._lexer[-1].chars.copy()
        return li


def _create_states(patterns, wildcard_mode):
    patterns = sorted(patterns, key=lambda p: len(p.value), reverse=True)
    states = [{}]

    if wildcard_mode:
        states.append({})
        states[0][None] = _Transition(True, "", 1)
        states[1][None] = _Transition(True, "", 1)

    for pattern in patterns:
        current_index = 0

        for value in pattern.value[:-1]:
            if value in states[current_index]:
                current_index = states[current_index][value].next_state
            else:
                new_index = len(states)
                transition = _Transition(True, "", new_index)
                states[current_index][value] = transition
                states.append({})
                current_index = new_index

        value = pattern.value[-1]
        if value in states[current_index]:
            states[states[current_index][value].next_state][None] = (
                _Transition(False, pattern.name, 0))
        else:
            states[current_index][value] = (
                _Transition(True, pattern.name, 0))

        if wildcard_mode:
            states[1][pattern.value[0]] = _Transition(False, "wildcard", 0)

    if wildcard_mode:
        for state in states:
            if None not in state:
                state[None] = _Transition(False, "", 1)

    return states


class Lexer:

    def __init__(self, patterns, config=Config()):
        self._states = _create_states(patterns, config.wildcard_mode)
        self._config = config

        self.chars = None

        self._current_lexer = self

    def __iter__(self):
        return self

    def __call__(self, chars):
        self.chars = chars

        token, chars = self._next(self.chars)
        self.chars = chars

        if token.name in self._config.drop_token_names:
            return next(self)

        if self._config.join_wildcards and token.name == "wildcard":
            chars_copy = self.chars.copy()
            try:
                next_token = next(self)
                if next_token.name == "wildcard":
                    return Token("wildcard", token.value + next_token.value)
                self.chars = chars_copy
            except StopIteration:
                pass

        return token

    def __next__(self):
        token, chars = self._next(self.chars)
        self.chars = chars

        if token.name in self._config.drop_token_names:
            return next(self)

        if self._config.join_wildcards and token.name == "wildcard":
            chars_copy = self.chars.copy()
            try:
                next_token = next(self)
                if next_token.name == "wildcard":
                    return Token("wildcard", token.value + next_token.value)
                self.chars = chars_copy
            except StopIteration:
                pass

        return token

    def _next(self, chars):
        token = None
        index = 0
        lexeme = []
        intermediate_token = None
        intermediate_chars = None

        def is_intermediate_token_only_valid_option(char):
            state = self._states[index]

            no_wildcard = None not in state or not state[None].emit
            no_valid_transition = char not in state and no_wildcard

            return no_valid_transition and intermediate_token

        def is_valid_intermediate_token():
            idx = index != 0
            none = None in self._states[index]
            emit = none and self._states[index][None].emit

            return idx and emit

        def create_intermediate_token(chars):
            return (Token(self._states[index][None].emit, "".join(lexeme)),
                    chars.copy())

        while True:
            try:
                char = chars.peek()
            except StopIteration:
                if intermediate_token is not None:
                    return intermediate_token, intermediate_chars
                if self._config.wildcard_mode and lexeme:
                    return Token("wildcard", "".join(lexeme)), chars.copy()

                raise

            state = self._states[index]

            if char == self._config.escape_character:
                # TODO: handle backslash at the end
                next(chars)
                char = "there is no char"

            if is_intermediate_token_only_valid_option(char):
                return intermediate_token, intermediate_chars

            transition = state[char if char in state else None]

            if transition.read:
                lexeme.append(next(chars))

            if transition.emit:
                token = Token(transition.emit, "".join(lexeme))

                break

            index = transition.next_state

            if is_valid_intermediate_token():
                intermediate_token, intermediate_chars = (
                    create_intermediate_token(chars))

        return token, chars

    def process(self, chars):
        self.chars = chars
        tokens = list(self)

        return tokens, self.chars


def create_lexer_iterator(config, trees, pattern_strings,
                          ConfigType=Config,
                          LexerType=Lexer,
                          LexerIteratorType=LexerIterator):
    lexers = {}

    def create_lexer(tree, lexers, start=False):
        kvs = zabu.util.extract_key_value_pairs(tree)

        config = zabu.util.create_config_with_values_from_dict(kvs, ConfigType)

        name = None if start else config.name

        patterns = [Pattern(s, s) for s in pattern_strings[name]
                    if s != "wildcard"]

        for drop_token_name in config.drop_token_names:
            patterns.append(Pattern(drop_token_name, drop_token_name))

        lexers[name] = LexerType(patterns, config)

    create_lexer(config, lexers, True)

    for tree in trees:
        create_lexer(tree, lexers)

    # TODO: remove MagicMock after removal of sublexer
    return LexerIteratorType(lexers, None, unittest.mock.MagicMock())
