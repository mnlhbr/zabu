import typing


class Iterator:

    def __init__(self, tokens, index=0):
        self._tokens = tokens
        self._index = index

    def __iter__(self):
        return self

    def __next__(self):
        token = self._get_token()

        self._index += 1

        return token

    def _get_token(self):
        if self._index >= len(self._tokens):
            raise StopIteration()

        return self._tokens[self._index]

    def copy(self):
        return Iterator(self._tokens, self._index)

    def peek(self):
        return self._get_token()


def create_config_with_values_from_dict(values, config_type):
    types = typing.get_type_hints(config_type)

    matches = {}

    for name in types:
        if name in values:
            if types[name] == typing.List[str]:
                matches[name] = values[name]
            elif types[name] == typing.List[int]:
                matches[name] = [int(value) for value in values[name]]
            elif types[name] == bool:
                matches[name] = values[name] in ("True", "true", "yes", "on")
            else:
                matches[name] = types[name](values[name])

    return config_type(**matches)


def extract_key_value_pairs(tree):
    def get_lexeme(node, index=0):
        return node.content[index].content

    def get_value(node):
        if node.name == "sqvalue":
            return get_lexeme(node, 1)
        return get_lexeme(node)

    pairs = {}

    for entry in tree:
        if entry.name != "keyvalue":
            continue

        key = get_lexeme(entry.content[0])
        values = [get_value(value) for value in entry.content[2:-1]]

        pairs[key] = values if len(values) > 1 else values[0]

    return pairs
