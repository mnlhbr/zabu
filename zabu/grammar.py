from dataclasses import dataclass
import collections
import functools
import typing
from zabu import lexer
from zabu import parser


@dataclass
class Rule:
    name: str
    visible: bool
    production: list
    node_name: str = ""

    def get_node_name(self):
        if not self.visible:
            return ""
        if self.node_name:
            return self.node_name
        return self.name


def create_rules(tree, transform_rhs=None):
    def dissect(element):
        content = {e.name: e for e in element.content}

        lhs = {e.name: e for e in content["lhs"].content}
        rhs = content["rhs"].content

        name = lhs["name"].content[0].content

        return name, lhs, rhs

    start = None

    rules = collections.defaultdict(list)

    for element in tree:
        if element.name != "rule":
            continue

        name, lhs, rhs = dissect(element)

        if start is None:
            start = name

        if transform_rhs is not None:
            rhs = transform_rhs(rhs)

        node_name = (lhs["node_name"].content[0].content
                     if "node_name" in lhs
                     else "")

        rules[name].append(Rule(name, "invisible" not in lhs, rhs, node_name))

    return start, rules


def convert_from_infix_to_postfix(expression, operands, operators, default_op,
                                  left_paren="", right_paren="",
                                  pre_operators=None, post_operators=None):
    if pre_operators is None:
        pre_operators = []
    if post_operators is None:
        post_operators = []

    expression = list(reversed(expression))
    stack = []
    output = []

    operators.update({pre: 2**16 - 1 for pre in pre_operators})

    last_symbol_was_operand = False

    def push_operator(operator):
        name = operator.name

        while (stack and stack[-1].name in operators and
               (operators[stack[-1].name] > operators[name] or
                operators[stack[-1].name] == operators[name] and
                stack[-1].name != name)):
            output.append(stack.pop())

        stack.append(operator)

    while expression:
        name = expression[-1].name

        if name in operands:
            if last_symbol_was_operand:
                push_operator(default_op)

            output.append(expression.pop())
            last_symbol_was_operand = True

        elif name == left_paren:
            stack.append(expression.pop())

        elif name == right_paren:
            while stack[-1].name != left_paren:
                output.append(stack.pop())
            stack.pop()
            expression.pop()

        elif name in post_operators:
            output.append(expression.pop())

        elif name in pre_operators:
            stack.append(expression.pop())

        else:
            last_symbol_was_operand = False
            push_operator(expression.pop())

    output.extend(reversed(stack))

    return output


class GrammarParts(typing.NamedTuple):
    config: list = []
    rules: list = []
    lexer: list = []


def extract_grammar_parts(tree):
    config, rules, lexer_ = [], [], []

    for node in tree:
        if node.name == "config":
            config = node.content
        elif node.name == "rules":
            rules = node.content
        elif node.name == "lexer":
            lexer_.append(node.content)

    return GrammarParts(config, rules, lexer_)


def create_parser(start, rules, lps=None):
    nonterminals = {start: parser.Placeholder()}
    terminals = {}
    pattern_strings = []

    lexer_patterns = lps if lps is not None else {None: set()}
    lexer_stack = [None]

    terminals["wildcard"] = parser.Terminal("wildcard")

    parser_ = _create_parser(start, rules,
                             nonterminals, terminals,
                             pattern_strings,
                             lexer_patterns,
                             lexer_stack)
    nonterminals[start].set_object(parser_)

    return parser_, pattern_strings


def _create_parser(start, rules, nonterminals, terminals, pattern_strings,
                   lexer_patterns, lexer_stack):
    def create_greedy(name, type_):
        count = 0
        content = [rhs.pop()]
        while tokens and tokens[-1].name == name:
            count += 1
            tokens.pop()
            content.append(rhs.pop())
        rhs.append(type_(list(reversed(content))))

    productions = []

    for rule in rules[start]:
        tokens = list(reversed(rule.production))

        rhs = []

        while tokens:
            token = tokens[-1]

            if token.name == "terminal":
                name = token.content[1].content
                if name in terminals:
                    rhs.append(terminals[name])
                    tokens.pop()
                else:
                    terminals[name] = parser.Terminal(name)
                    pattern_strings.append(name)
                lexer_patterns[lexer_stack[-1]].add(name)

            elif token.name == "nonterminal":
                name = token.content[0].content
                if name in nonterminals:
                    rhs.append(nonterminals[name])
                    tokens.pop()
                else:
                    nonterminals[name] = parser.Placeholder()
                    nonterminals[name].set_object(_create_parser(
                        name, rules, nonterminals, terminals, pattern_strings,
                        lexer_patterns, lexer_stack))

            elif token.name == "and":
                create_greedy("and", parser.Production)

            elif token.name == "or":
                create_greedy("or", parser.Option)

            elif token.name == "repetition01":
                rhs.append(parser.Repetition(rhs.pop(), 0, 1))
                tokens.pop()

            elif token.name == "repetition0inf":
                rhs.append(parser.Repetition(rhs.pop(), 0))
                tokens.pop()

            elif token.name == "repetition1inf":
                rhs.append(parser.Repetition(rhs.pop(), 1))
                tokens.pop()

            elif token.name == "change_lexer":
                lexer_name = token.content[1].content[0].content
                if lexer_name == " ":
                    lexer_name = None
                    lexer_stack.pop()
                else:
                    lexer_stack.append(lexer_name)
                rhs.append(parser.LexerChanger(lexer_name))
                tokens.pop()

                if lexer_stack[-1] not in lexer_patterns:
                    lexer_patterns[lexer_stack[-1]] = set()

        name = rule.get_node_name()

        productions.append(parser.Production(rhs, name))

    if len(productions) > 1:
        return parser.Option(productions)

    return productions[0]


def _create_transform_function():
    transform_cfg = {}
    transform_cfg["operands"] = (
        set(("terminal", "nonterminal", "change_lexer")))
    transform_cfg["operators"] = {"and": 1, "or": 0}
    transform_cfg["default_op"] = parser.Node("and", "")
    transform_cfg["left_paren"] = "leftparenthesis"
    transform_cfg["right_paren"] = "rightparenthesis"
    transform_cfg["pre_operators"] = set()
    transform_cfg["post_operators"] = (
        set(("repetition01", "repetition0inf", "repetition1inf")))

    return functools.partial(convert_from_infix_to_postfix, **transform_cfg)


def create_front_end(tree):
    parts = extract_grammar_parts(tree)

    start, rules = create_rules(parts.rules, _create_transform_function())

    lexer_pattern_strings = {None: set()}
    parser_, pattern_strings = create_parser(start,
                                             rules,
                                             lexer_pattern_strings)

    lexer_iterator = lexer.create_lexer_iterator(parts.config, parts.lexer,
                                                 lexer_pattern_strings)

    return lexer_iterator, parser_
